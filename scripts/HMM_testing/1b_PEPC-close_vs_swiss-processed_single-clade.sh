#!/bin/bash 

clist="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/1_PEPC-close/PEPC-close_clade-list.txt" 
out="/g/scb/bork/eckey/cfix/data/dataprep/01f_test_vs_swiss/1_PEPC-close"

for clade in $(cat $clist); do 
    hmm=/g/scb/bork/eckey/cfix/data/dataprep/01e_HMMs/1_PEPC-close/$clade/$clade.final.hmm

    grep "GA" $hmm > $out/$clade.out.processed
    hitout=$out/$clade.out.processed
    ga_value=$(grep "GA" $hitout | head -n 1 | awk '{print $2}')

    awk -v ga_value="$ga_value" \
    'BEGIN {print sprintf("%-110s\t%-20s\t%-15s\t%-10s\t%-10s", "target_name", "query_name", "E-value", "score", "over_GA")} \
    NR>3 {
    name_truncated = gensub(/OS=.*/, "", "g", $1)


    over_GA = ($6 >= ga_value) ? "*" : ""; 
    
    printf "%-110s\t%-20s\t%-15s\t%-10s\t%-10s\n", name_truncated, $3, $5, $6, over_GA 

    }' $out/$clade.out >> $hitout
done 

    