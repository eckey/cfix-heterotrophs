#!/bin/bash
#SBATCH --job-name=PEPCswiss
#SBATCH -n "1"
#SBATCH --mem 1G
#SBATCH -t 0-2:00:00
#SBATCH -o PC-close_vs_swiss.%j.%N.out
#SBATCH -e PC-close_vs_swiss.%j.%N.err

module load hmmer/3.3.1

# Parse command-line options for clade
CLADE=""
while getopts 'c:' option; do
    case "${option}" in
        c) CLADE=${OPTARG} ;;  # -c specifies the target clade
        *) echo "Usage: $0 -c <clade_name>" >&2; exit 1 ;;
    esac
done

swiss="/g/scb/bork/eckey/cfix/data/Control-databases/swiss_sanitized.fasta"
out="/g/scb/bork/eckey/cfix/data/dataprep/01f_test_vs_swiss/1_PEPC-close"

# for loop iterating through clade-list file 
    
indir="/g/scb/bork/eckey/cfix/data/dataprep/01e_HMMs/1_PEPC-close/$CLADE"
hmm="$indir/$CLADE.final.hmm"
    
    # run hmm search agianst swiss-prot database 
hmmsearch \
    --tblout $out/$CLADE.out \
    --noali \
    --cpu ${SLURM_NTASKS} \
    ${hmm} $swiss    


