#!/bin/bash
#SBATCH --job-name=ProG_pros
#SBATCH -n "2"
#SBATCH --mem 4G
#SBATCH -t 0-2:00:00
#SBATCH -o ProG_processing.%j.%N.out
#SBATCH -e ProG_processing.%j.%N.err

dir="/g/scb/bork/eckey/cfix/data/profiles_progenomes_round1/output"
input_file="carboxylase_issue2737_fr13_best_hit.out"
output_file="progenomes_taxonomy_out.tsv"
cd ${dir}

source /g/scb/bork/eckey/software/miniforge3/etc/profile.d/conda.sh
conda activate dbpython

scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}
cd ${scratch_dir}

cp ${dir}/${input_file} ${scratch_dir}
python3 /g/scb/bork/eckey/cfix/scripts/Search_ouput_analysis/4b_Progenomes_taxonomy.py "${scratch_dir}/${input_file}" "${scratch_dir}/${output_file}"

cp ${scratch_dir}/${output_file} ${dir}/ 

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}



