#!/usr/bin/python3

import sys
import os
import pandas as pd
import pymongo 

client = pymongo.MongoClient('mongodb://eckey:password_eckey@koppa:26017')
db = client['progenomes']

input_file = sys.argv[1]
output_file = sys.argv[2]

proG_hits = pd.read_csv(input_file, delimiter='\t')
rel_col = proG_hits.iloc[:,[0,2,5]]
rel_col.columns = ['org_names', 'clade', 'score']
rel_col[['sample_group_id', 'genome_id', 'fasta_header']] = rel_col['org_names'].str.split(';', expand=True)
rel_col = rel_col.drop('org_names', axis=1)
rel_col = rel_col[['sample_group_id', 'genome_id', 'fasta_header', 'clade', 'score']]

def get_taxonomy_info(df):
    classification = ['domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    	
    for col in classification:
        df[col] = 'N/A'
    
    for idx, row in df.iterrows():
        genome_id = row['genome_id']
        sample = db.samples.find_one({'sample_id': genome_id})['gtdbtk_v211']
        for level in classification:
            sample_tax = sample.get(level, 'N/A') 
            df.at[idx, level] = sample_tax 
    return df 

processed_hits = get_taxonomy_info(rel_col)
processed_hits.to_csv(output_file, sep='\t', index=False)






