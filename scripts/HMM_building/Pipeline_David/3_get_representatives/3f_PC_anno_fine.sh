#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PCanno

# Define number of cores to use
#SBATCH -n 2

# Define memory amount (1x cores in GB)
#SBATCH --mem 2G

# Define maximum run time
#SBATCH -t 1-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01g_representative_sequences/02_PC-close/rep_tree/rep_tree_files"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01g_representative_sequences/02_PC-close/rep_tree"

# Define the name of the output annotation file
annotation_file="${output_dir}/PC_anno_fine.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/PC-clade1_seq_0.5_rep_seq.fasta"              # PC                1
    "${input_dir}/PC-clade2_seq_0.5_rep_seq.fasta"              #                   2
    "${input_dir}/PCa-clade1_seq_0.5_rep_seq.fasta"             # PC a              3
    "${input_dir}/PCa-clade2_seq_0.5_rep_seq.fasta"             #                   4
    "${input_dir}/PCb-clade_seq_0.5_rep_seq.fasta"              # PC b              5

    "${input_dir}/ACC1-2-clade_seq_0.5_rep_seq.fasta"           # ACC 1             6
    "${input_dir}/ACC2-clade1_seq_0.5_rep_seq.fasta"            # ACC 2             7
    "${input_dir}/ACC2-clade2_seq_0.5_rep_seq.fasta"            #                   8
    "${input_dir}/ACC2-clade3_seq_0.5_rep_seq.fasta"            #                   9
    "${input_dir}/ACC2-clade4_seq_0.5_rep_seq.fasta"            #                   10
    "${input_dir}/ACC-trabsf-a-clade_seq_0.5_rep_seq.fasta"     # ACC transf a      11  
    "${input_dir}/ACC-transf-b-clade_seq_0.5_rep_seq.fasta"     # ACC transf b      12
    "${input_dir}/ACC2-carrier-clade_seq_0.5_rep_seq.fasta"     # ACC carrier       13

    "${input_dir}/PCCa-clade_seq_0.5_rep_seq.fasta"             # Prop-CoA a        14
    "${input_dir}/PCCb-clade1_seq_0.5_rep_seq.fasta"            # Prop-CoA b        15
    "${input_dir}/PCCb-clade2_seq_0.5_rep_seq.fasta"            #                   16
    "${input_dir}/PCCb-clade3_seq_0.5_rep_seq.fasta"            #                   17
    "${input_dir}/PCCb-clade4_seq_0.5_rep_seq.fasta"            #                   18

    "${input_dir}/MCCa-clade1_seq_0.5_rep_seq.fasta"            # MCC a             19
    "${input_dir}/MCCa-clade2_seq_0.5_rep_seq.fasta"            #                   20
    "${input_dir}/MCCa-clade3_seq_0.5_rep_seq.fasta"            #                   21
    "${input_dir}/MCCb-clade1_seq_0.5_rep_seq.fasta"            #                   22
    "${input_dir}/MCCb-clade2_seq_0.5_rep_seq.fasta"            #                   23
    "${input_dir}/MCCb-clade3_seq_0.5_rep_seq.fasta"            #                   24
    "${input_dir}/MCCb-clade4_seq_0.5_rep_seq.fasta"            #                   25

    "${input_dir}/GCCa-clade_seq_0.5_rep_seq.fasta"             #                   26
    "${input_dir}/GCCb-clade_seq_0.5_rep_seq.fasta"             #                   27

    "${input_dir}/ACC-PCC-K01964-clade_seq_0.5_rep_seq.fasta"   # ACC-PCC           28
    "${input_dir}/ACC-PCC-K15036-clade_seq_0.5_rep_seq.fasta"   # ACC-PCC           29
    "${input_dir}/ACC-PCC-K18472-clade_seq_0.5_rep_seq.fasta"   # ACC-PCC transf    30
    "${input_dir}/ACC-PCC-K18603-clade_seq_0.5_rep_seq.fasta"   # ACC-PCC           31
    "${input_dir}/ACC-PCC-K19321-clade_seq_0.5_rep_seq.fasta"   #                   32
    "${input_dir}/ACC-PCC-K19604-clade_seq_0.5_rep_seq.fasta"   #                   33
    "${input_dir}/ACC-PCC-K22568-clade_seq_0.5_rep_seq.fasta"   #                   34

    "${input_dir}/PC-close_discard-seq-Knum_0.5_rep_seq.fasta"  #                   35
)


# Define the colors for each FASTA file
colors=(
    "#4d1300"  # Color for PC           red/orange      1  15%
    "#992600"  #                                        2  30%
    "#e63900"  # Color for PC a                         3  45%
    "#ff6633"  #                                        4  60%
    "#ff9f80"  # Color for PC b                         5  75%

    "#666600"  # Color for AAC1-2-clade   yellow        6  20%
    "#cccc00"  # Color for ACC 2                        7  30%
    "#cccc00"  #                                        8  40%
    "#ffff00"  #                                        9  50%
    "#ffff33"  #                                        10 60%
    "#ffff66"  #                                        11 70%      
    "#ffff99"  #                                        12 80%
    "#ffffcc"  #                                        13 90%

    "#00661a"  # Color for Prop-CoA a   green           14 20%
    "#00b32d"  # Color for Prop-CoA b                   15 35%
    "#00ff40"  #                                        16 50%   
    "#4dff79"  #                                        17 65%
    "#99ffb3"  #                                        18 80%

    "#003366"  # Color for MCC a        blue            19 20%
    "#004d99"  #                                        20 30%
    "#0066cc"  #                                        21 40%
    "#0080ff"  # Color for MCC b                        22 50%
    "#3399ff"  #                                        23 60%
    "#66b3ff"  #                                        24 70%
    "#99ccff"  #                                        25 80%

    "#4d0099"  # Color for GCC a        violett         26 30%
    "#7300e6"  # Color for GCC b                        27 45%

    "#330033"  # Color for ACC-PCC      pink            28 10%     K01964
    "#660066"  # Color for ACC-PCC                      29 20%     K15036
    "#990099"  # Color for ACC-PCC                      30 30%     K15052
    "#cc00cc"  # Color for ACC-PCC                      31 40%     K18472
    "#ff00ff"  # Color for ACC-PCC                      32 50%     K18603
    "#ff33ff"  # Color for ACC-PCC                      33 60%     K18604
    "#ff66ff"  # Color for ACC-PCC                      34 70%     K19321
    
    "#808080"  #                        grey            35
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


