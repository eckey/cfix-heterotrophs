#!/bin/bash
# Define job name
#SBATCH --job-name=PEPCanno

# Define number of cores to use
#SBATCH -n 2

# Define memory amount (1x cores in GB)
#SBATCH --mem 2G

# Define maximum run time
#SBATCH -t 0-01:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01g_representative_sequences/01_PEPC-close/rep_tree/rep_tree_files"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01g_representative_sequences/01_PEPC-close/rep_tree"

# Define the name of the output annotation file
annotation_file="${output_dir}/PEPC_anno_fine.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/PC-clade1_seq_0.5_rep_seq.fasta" # PC 
    "${input_dir}/PC-clade2_seq_0.5_rep_seq.fasta"
    "${input_dir}/PCa-clade1_seq_0.5_rep_seq.fasta" # PC a
    "${input_dir}/PCa-clade2_seq_0.5_rep_seq.fasta"
    "${input_dir}/PCb-clade1_seq_0.5_rep_seq.fasta" # PC b
    "${input_dir}/PCCa-clade_seq_0.5_rep_seq.fasta" # Prop-CoA a
    "${input_dir}/PCCb-clade_seq_0.5_rep_seq.fasta" # Prop-CoA b
    "${input_dir}/PEPC-clade1_seq_0.5_rep_seq.fasta" # PEP-C 
    "${input_dir}/PEPC-clade2_seq_0.5_rep_seq.fasta" 
    "${input_dir}/ACa-clade_seq_0.5_rep_seq.fasta" # AC a
    "${input_dir}/ACb-clade_seq_0.5_rep_seq.fasta" # AC b
    "${input_dir}/ACg-clade_seq_0.5_rep_seq.fasta" # AC g
    "${input_dir}/PEPC-close_discard-seq-Knum_0.5_rep_seq.fasta" #discarded
    )

# Define the colors for each FASTA file
colors=(
    "#2b0808"  # Color for PC           10% red
    "#6b1414"  #                        25%
    "#c12525"  # Color for PC a         45%
    "#de5454"  #                        60%
    "#f3bfbf"  # Color for PC b         80%
    "#bf8040"  # Color for Prop-CoA a   50% brown
    "#d2a679"  # Color for Prop-CoA b   65%
    "#80d629"  # Color for PEPC         50% green
    "#a6e269"  #                        65%
    "#d6d629"  # Color for AC a         50% yellow
    "#e2e269"  # Color for AC b         65%
    "#efefa9"  # Color for AC g         80%
    "#808080"  # discarded                  grey 

)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done

