#!/bin/bash -x
#SBATCH -n 1
#SBATCH --mem 1G
#SBATCH -t 0-2:00:00

while getopts 'c:d:' option; do
  case "${option}" in
    c) clade_list=${OPTARG};;
    d) dir=${OPTARG};;
  esac
done

# Ensure the output file is created or cleared
touch "${dir}/clades_over-threshold.txt"
out="${dir}/clades_over-threshold.txt"

# Read each clade from the list and check for *.final.hmm files
while IFS= read -r clade; do

  if ls "${dir}/$clade"/*.final.hmm > /dev/null 2>&1; then
    echo "$clade" >> "$out"
  else
    echo "$clade did not pass threshold" >> "$out" 
  fi
  
done < "${clade_list}"
