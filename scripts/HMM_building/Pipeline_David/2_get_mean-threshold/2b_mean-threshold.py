import kaleido  
import os
import pandas as pd
from statistics import mean
import argparse
from glob import glob
import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio


# Function to extract optimal thresholds and metrics from GA.tsv
def select_thresholds(base_dir, clade):
    eval_dir = os.path.join(base_dir, clade, "eval")
    os.makedirs(eval_dir, exist_ok=True)
    optimal_metrics_file = os.path.join(eval_dir, "optimal_metrics.tsv")

    optimal_metrics = []
    folds = glob(os.path.join(base_dir, clade, "output", "fold*"))
    for fold_dir in folds:
        fold_name = os.path.basename(fold_dir)
        ga_file = os.path.join(fold_dir, "GA.tsv")
        
        # Check if the GA file exists and is not empty
        if not os.path.exists(ga_file):
            print(f"Warning: GA.tsv file not found for fold {fold_name}. Skipping.")
            continue
        else:
            # Debug statement for GA.tsv file found
            print(f"Debug: Found GA file for fold {fold_name}: {ga_file}")
        
        df_ga = pd.read_csv(ga_file, sep="\t")
        
        # Check if there's a row with the '*' in max_mcc column
        optimal_row = df_ga[df_ga["max_mcc"] == "*"]
        if not optimal_row.empty:
            optimal_row = optimal_row.iloc[0]  # Take the first match (if any)
            optimal_metrics.append({
                "FOLD": fold_name,
                "THRESHOLD": optimal_row["threshold"],
                "TPR": optimal_row["tpr"],
                "FPR": optimal_row["fpr"],
                "MCC": optimal_row["mcc"]
            })
            
    for entry in optimal_metrics:
        if entry["MCC"] >= 0.8:
            entry["SELECTED"] = "*"

    # Save optimal metrics to file
    if optimal_metrics:
        pd.DataFrame(optimal_metrics).to_csv(optimal_metrics_file, sep="\t", index=False)
    else:
        print("Warning: No optimal thresholds were found in any folds.")


# Function to generate plots for TPR vs FPR and MCC distribution
def generate_plots(base_dir, clade):
    eval_dir = os.path.join(base_dir, clade, "eval")
    optimal_metrics_file = os.path.join(eval_dir, "optimal_metrics.tsv")
    plot_tpr_vs_fpr = os.path.join(eval_dir, "tpr_vs_fpr.png")
    plot_mcc = os.path.join(eval_dir, "mcc_distribution.png")

    # Read the optimal metrics
    df_metrics = pd.read_csv(optimal_metrics_file, sep="\t")

    # Plot TPR vs FPR
    pio.templates.default = "plotly_white"
    fig1 = px.scatter(
        df_metrics,
        x="FPR",
        y="TPR",
        color="FOLD",
        title="TPR vs FPR",
        labels={"FPR": "False Positive Rate", "TPR": "True Positive Rate"},
        template="simple_white",
        symbol="FOLD"
    )
    fig1.update_traces(marker=dict(size=15, line=dict(width=1, color='DarkSlateGrey')))
    fig1.add_shape(type="line", x0=0, y0=0, x1=1, y1=1, line=dict(color="DarkSlateGrey", width=1, dash="dash"))
    fig1.write_image(plot_tpr_vs_fpr)

    # Plot MCC distribution with dynamic bin size
    mcc_values = df_metrics["MCC"]
    if len(mcc_values.unique()) > 1:  # Adjust bin size only if there's variation
        bin_size = (mcc_values.max() - mcc_values.min()) / 10  # Divide range into 10 bins
    else:
        bin_size = 0.1  # Default small bin size if all values are identical

    fig2 = px.histogram(
        df_metrics,
        x="MCC",
        title="MCC Distribution Across Folds",
        labels={"MCC": "Matthews Correlation Coefficient"},
        template="simple_white",
    )
    fig2.update_traces(marker=dict(line=dict(width=1, color='DarkSlateGrey')))
    fig2.update_xaxes(dtick=bin_size)  # Dynamically set x-axis ticks based on bin size
    fig2.update_xaxes(range=[mcc_values.min() - bin_size, mcc_values.max() + bin_size])  # Adjust range
    fig2.write_image(plot_mcc)


# Function to set the final threshold in the HMM profile
def set_final_threshold(base_dir, clade):
    eval_dir = os.path.join(base_dir, clade, "eval")
    optimal_metrics_file = os.path.join(eval_dir, "optimal_metrics.tsv")
    final_hmm = os.path.join(base_dir, clade, f"{clade}.final.hmm")
    profile_hmm = os.path.join(base_dir, clade, "output", "fold1/profiles/profile.hmm")

    # Read the optimal metrics
    optimal_metrics = pd.read_csv(optimal_metrics_file, sep="\t")
    
    # Filter for only the rows where "selected" == "*"
    selected_metrics = optimal_metrics[optimal_metrics["SELECTED"] == "*"]

    if not selected_metrics.empty:
        # Calculate the mean optimal threshold across all selected folds
        final_threshold = mean(selected_metrics["THRESHOLD"])

        with open(profile_hmm) as infile, open(final_hmm, "w") as outfile:
            for line in infile:
                if line.startswith("CKSUM"):
                  outfile.write(f"GA    {final_threshold} {final_threshold};\n")
                else:
                    outfile.write(line)
    else:
        print("Warning: No selected thresholds found with MCC >= 0.8. Final threshold not calculated.")


#


# Main function to parse arguments and execute steps
def main():
    parser = argparse.ArgumentParser(description="Mean threshold gathering")
    parser.add_argument("-c", "--clade", required=True, help="Clade name")
    parser.add_argument("-b", "--base_dir", required=True, help="Base directory for the workflow")
    args = parser.parse_args()

    select_thresholds(args.base_dir, args.clade)
    generate_plots(args.base_dir, args.clade)
    set_final_threshold(args.base_dir, args.clade)


if __name__ == "__main__":
    main()
