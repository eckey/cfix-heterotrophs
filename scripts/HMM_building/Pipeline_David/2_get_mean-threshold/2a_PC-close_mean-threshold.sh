#!/bin/bash
#SBATCH --job-name=PC-mean
#SBATCH -n 1
#SBATCH --mem 1G 
#SBATCH -t 2-0:00:00
#SBATCH -o mt-PC.%j.%N.out

module load Kaleido/0.2.1-GCCcore-13.3.0

while getopts 'c:' option
do
case "${option}"
in
c) clade=${OPTARG};;
esac
done

dir="/g/scb/bork/eckey/cfix/data/dataprep/01e_HMMs/2_PC-close"

source /g/bork3/home/fullam/miniconda3/etc/profile.d/conda.sh
conda activate we
wait

mkdir $dir/$clade/eval
python3 /g/scb/bork/eckey/cfix/scripts/HMM_building/Pipeline_David/2_get_mean-threshold/2b_mean-threshold.py \
    -c ${clade} \
    -b ${dir}

conda deactivate
