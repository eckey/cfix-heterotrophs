#!/bin/bash
#SBATCH --job-name=meanthresh
#SBATCH -n 4
#SBATCH --mem 4G 
#SBATCH -t 2-0:00:00
#SBATCH -o mt-PCPEPC.%j.%N.out

dir="/g/scb/bork/eckey/cfix/data/dataprep/01f_HMMs/HMMs_based_PC-PEPC"
clist="/g/scb/bork/eckey/cfix/data/dataprep/01e_clean-up/PC-PEPC_close2.1bb_clean/PC-PEPC_clade-list.txt"

source /g/scb/bork/eckey/software/miniforge3/etc/profile.d/conda.sh
conda activate kfoldpipe 
wait

# for loop iterating through clade-list file 
for clade in $(cat ${clist}); do 
    mkdir $dir/$clade/eval
    python3 /g/scb/bork/eckey/cfix/scripts/HMM_building/Pipeline_David/2_get_mean-threshold/2b_mean-threshold.py \
        -c ${clade} \
        -b ${dir}

done



     
