#!/bin/bash
#SBATCH --job-name=meanthresh
#SBATCH -n 8
#SBATCH --mem 16G 
#SBATCH -t 2-0:00:00
#SBATCH -o mt-MCGC.%j.%N.out

dir="/g/scb/bork/eckey/cfix/data/dataprep/01f_HMMs/HMMs_based_MC-GC"
clist="/g/scb/bork/eckey/cfix/data/dataprep/01e_clean-up/MCC_GCC_clean/MCC_GCC_clade-list.txt"

source /g/scb/bork/eckey/software/miniforge3/etc/profile.d/conda.sh
conda activate kfoldpipe 
wait

# for loop iterating through clade-list file 
for clade in $(cat ${clist}); do 
    mkdir $dir/$clade/eval
    python3 /g/scb/bork/eckey/cfix/scripts/HMM_building/Pipeline_David/2_get_mean-threshold/2b_mean-threshold.py \
        -c ${clade} \
        -b ${dir}

done
