import kaleido                                 # For saving plots as images
import os                                      # For directory and file path operations
import argparse                                # For command-line argument parsing
import pandas as pd                            # For handling tabular data
import math                                    # For mathematical calculations
import plotly.express as px                    # For creating plots
import plotly.io as pio                        # For Plotly input/output operations
import plotly.graph_objects as go              # For building detailed plotly visualizations
import matplotlib.pyplot as plt

# Parse command-line arguments
def parse_args():
    parser = argparse.ArgumentParser(description="Generating overview of sequence assignment for k-fold cross-validation")
    parser.add_argument("-k", help="Number of folds", dest="K", type=int, required=True)  # Number of folds
    parser.add_argument("--dir", help="Processing directory", dest="DIR", type=str, required=True)  # Input/output directory
    args = parser.parse_args()
    return args

# Read classification data
def get_class(CLASS):
    classification = pd.read_csv(CLASS, sep='\t')  # Load classification file
    return classification

# Read training results from hmmsearch output
def get_train_results(HITS):
    train_out = pd.read_csv(HITS, sep='\t', header=None)  # Load hmmsearch results
    train_out = train_out.rename(columns={train_out.columns[0]: "NAME"})  # Rename first column to NAME
    train_out = train_out.rename(columns={train_out.columns[1]: "SCORE"})  # Rename second column to SCORE
    return train_out

# Calculate performance metrics
def calc_rates(classification, TRAIN_OUT):
    # Compute total positives (P) and negatives (N) in the training set
    P = len(classification[(classification["TYPE"] == 'TP') & (classification['CLASS'] == 'train')])
    N = len(classification[(classification["TYPE"] == 'FP') & (classification['CLASS'] == 'train')])

    # Initialize lists to store thresholds and metrics
    threshold = []
    mcc = []
    tpr = []
    fpr = []

    # Iterate through score thresholds (in steps of 5)
    for i in range(int(TRAIN_OUT['SCORE'].min()), int(TRAIN_OUT['SCORE'].max()), 5):
        merged = TRAIN_OUT.merge(classification, how='left')  # Merge training results with classification data
        merged = merged.loc[merged["SCORE"] >= i]            # Filter sequences above the threshold

        # Calculate counts for TP, FP, TN, and FN
        TP = len(merged[merged["TYPE"] == 'TP'])
        FP = len(merged[merged["TYPE"] == 'FP'])
        TN = (N - FP)
        FN = (P - TP)

        # Compute TPR and FPR
        TPR = TP / (TP + FN)
        FPR = FP / (FP + TN)

        # Compute MCC
        if (math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)) == 0):
            MCC = 0  # Handle division by zero
        else:
            MCC = (TP * TN - FP * FN) / math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN))

        # Append metrics to lists
        threshold.append(i)
        mcc.append(MCC)
        tpr.append(TPR)
        fpr.append(FPR)

    # Create a DataFrame with thresholds and metrics
    df = pd.DataFrame(list(zip(threshold, mcc, tpr, fpr)), columns=['threshold', 'mcc', 'tpr', 'fpr'])
    return df

# Identify the optimal threshold and save it
def generate_GA_df(df, GA_OUT):
    # Add a column to mark the row with the maximum MCC
    max_mcc_value = df['mcc'].max()
    # Filter rows with the maximum MCC value and find the one with the smallest threshold
    optimal_row = df[df['mcc'] == max_mcc_value].sort_values(by='threshold').iloc[0]
    df['max_mcc'] = df.apply(lambda row: '*' if (row['threshold'] == optimal_row['threshold'] and row['mcc'] == max_mcc_value) else '', axis=1)

    # Save the entire DataFrame to GA_OUT
    df.to_csv(GA_OUT, index=False, sep='\t')
    return df

# Generate and save ROC plot
def plot_results(df, GA, PLOT):
    pio.templates.default = "plotly_white"  # Use a white background for the plot
    roc = go.Figure()
    # Add ROC curve
    roc.add_trace(go.Scatter(
        x=df['fpr'],
        y=df['tpr'],
        mode='lines',
        name='ROC Curve',
        line=dict(color="turquoise"), 
        marker=dict(size=5)
    ))  
    # Highlight optimal threshold
    optimal_point = df[df['max_mcc'] == '*'].iloc[0]
    roc.add_trace(go.Scatter(
        x=[optimal_point['fpr']],
        y=[optimal_point['tpr']],
        mode='markers',
        name='Optimal Threshold',
        marker=dict(color="darkblue", size=10, symbol='circle')
    ))
    # Calculate x-axis limits with padding
    fpr_min = df['fpr'].min()
    fpr_max = df['fpr'].max()
    x_padding = 0.2 
    x_range = [max(-0.1, fpr_min - x_padding), min(1, fpr_max + x_padding)]
    # Update plot layout
    roc.update_layout(
        xaxis=dict(
            title="False Positive Rate (FPR)",
            range=x_range  # Apply the calculated range
        ),
        yaxis_title="True Positive Rate (TPR)",
        title="ROC Curve with Optimal Threshold",
        legend=dict(title="Legend"),
        template="plotly_white"
    )
    roc.write_image(PLOT, engine="kaleido")  # Use kaleido to save the plot as an image
    
def plot_MCC_curve(df, path): 
    pio.templates.default = "plotly_white"  # Use a white background for the plot
    mcc = go.Figure()
    # Add ROC curve
    mcc.add_trace(go.Scatter(
        x=df['threshold'],
        y=df['mcc'],
        mode='lines',
        name='MCC Curve',
        line=dict(color="turquoise"), 
        marker=dict(size=5)
    ))  
    # Highlight optimal threshold
    optimal_point = df[df['max_mcc'] == '*'].iloc[0]
    mcc.add_trace(go.Scatter(
        x=[optimal_point['threshold']],
        y=[optimal_point['mcc']],
        mode='markers',
        name='Optimal Threshold',
        marker=dict(color="darkblue", size=10, symbol='circle')
    ))
    # Calculate x-axis limits with padding
    thres_min = df['threshold'].min()
    thres_max = df['threshold'].max()
    x_padding = 0.1
    x_range = [max(0, thres_min - (thres_max * x_padding)), thres_max + (x_padding * thres_max)]
    # Update plot layout
    mcc.update_layout(
        xaxis_title="Threshold" ,
        yaxis_title="MCC value",
        title="MCC Curve with Optimal Threshold",
        legend=dict(title="Legend"),
        template="plotly_white",
        xaxis=dict(range=x_range)
    )
    mcc.write_image(path, engine="kaleido")
    

# Main function get and plot GA
def get_plot_GA():
    files = parse_args()  # Parse arguments
    for i in range(1, files.K+1):  # Loop through folds
        fold = "fold" + str(i)  # Fold name
        fold_table = fold + '.tsv'  # Classification file
        CLASS = os.path.join(files.DIR, fold, fold_table)
        classification = get_class(CLASS)
        HITS = os.path.join(files.DIR, fold, 'hmm_out/train.out.final')  # Training results file
        TRAIN_OUT = get_train_results(HITS)
        df = calc_rates(classification, TRAIN_OUT)  # Calculate metrics
        GA_OUT = os.path.join(files.DIR, fold, 'GA.tsv')  # File for optimal threshold
        GA = generate_GA_df(df, GA_OUT)
        PLOT = os.path.join(files.DIR, fold, 'GA.png')  # File for ROC plot
        plot_results(df, GA, PLOT)
        
        mcc_plot_path = os.path.join(files.DIR, fold, 'mcc_curve.png')  # Path for the MCC curve plot
        GA_DF = pd.read_csv(GA_OUT, sep='\t')  # Read GA.tsv as a DataFrame
        plot_MCC_curve(GA_DF, mcc_plot_path)
        







# define function write GA values to profile.thresh.hmm
def write_GA(thresh, hmm_in, hmm_out):
    # Read the threshold value from GA.tsv
    THRESH = pd.read_csv(thresh, index_col=None, sep='\t')  # Load threshold file
    THRESH = str(int(THRESH['threshold'].iloc[0]))  # Convert threshold to an integer string using iloc[0]

    # Open input HMM file for reading and output HMM file for writing
    HMM_IN = open(hmm_in)  # Open input HMM profile file
    HMM_OUT = open(hmm_out, 'w')  # Open output file to save updated HMM profile

    # Iterate through lines in the HMM input file
    for line in HMM_IN:
        if line.startswith('CKSUM'):  # If line starts with 'CKSUM'
            # Append the threshold values in HMM format
            HMM_OUT.write(line + 'GA    ' + THRESH + ' ' + THRESH + ';\n')
        else:
            # Write the line unchanged
            HMM_OUT.write(line)

    # Close the output file
    HMM_OUT.close()

# Create profile.thresh.hmm
def create_profile_GA():
    files = parse_args()  # Parse command-line arguments
    for i in range(1, files.K + 1):  # Loop through each fold (1 to k)
        fold = "fold" + str(i)  # Define fold name
        fold_table = fold + '.tsv'  # Filename for fold table (not directly used here)

        # Construct paths for threshold file, input HMM, and output HMM
        thresh = os.path.join(files.DIR, fold, 'GA.tsv')  # Path to GA.tsv file
        hmm_in = os.path.join(files.DIR, fold, 'profiles/profile.hmm')  # Input HMM profile
        hmm_out = os.path.join(files.DIR, fold, 'profiles/profile.thresh.hmm')  # Output HMM
        # Call write_GA to create the profile.thresh.hmm file
        write_GA(thresh, hmm_in, hmm_out)




# Entry point of the script
if __name__ == "__main__":
    get_plot_GA()
    create_profile_GA()