import os
import argparse
import pandas as pd
import math

# Parse command-line arguments
def parse_args():
    parser = argparse.ArgumentParser(description="Evaluating test run for k-fold cross-validation")
    parser.add_argument("-k", help="Number of folds", dest="K", type=int, required=True)  # Number of folds (integer)
    parser.add_argument("--dir", help="Processing directory", dest="DIR", type=str, required=True)  # Directory with data files
    args = parser.parse_args()  # Parse arguments
    return args  # Return the parsed arguments

# Load classification data (true positive/false positive labels)
def get_class(CLASS):
    classification = pd.read_csv(CLASS, sep='\t')  # Read classification file as dataframe
    return classification  # Return dataframe with classification data

# Load test results (sequence scores from HMM search)
def get_test_results(HITS):
    test_out = pd.read_csv(HITS, sep='\t', header=None)  # Read the HMM search result file (without header)
    test_out = test_out.rename(columns={test_out.columns[0]: "NAME", test_out.columns[1]: "SCORE"})  # Rename columns to 'NAME' and 'SCORE'
    return test_out  # Return the dataframe with test results

# Calculate performance metrics: TPR, FPR, MCC
def calc_rates(classification, test_out, EVAL_OUT):
    tpr = []  # List to store True Positive Rates
    fpr = []  # List to store False Positive Rates
    mcc = []  # List to store Matthews Correlation Coefficients

    # Calculate number of positives (P) and negatives (N) in the classification
    P = len(classification[(classification["TYPE"] == 'TP') & (classification['CLASS'] == 'test')])
    N = len(classification[(classification["TYPE"] == 'FP') & (classification['CLASS'] == 'test')])

    merged = test_out.merge(classification, how='left')  # Merge test results with classification data based on 'NAME'
    TP = len(merged[merged["TYPE"] == 'TP'])  # True positives
    FP = len(merged[merged["TYPE"] == 'FP'])  # False positives
    TN = (N - FP)  # True negatives
    FN = (P - TP)  # False negatives

    # Calculate TPR and FPR
    TPR = TP / (TP + FN)  # True Positive Rate
    FPR = FP / (FP + TN)  # False Positive Rate

    # Calculate MCC (Matthews Correlation Coefficient)
    if (math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)) == 0):
        mcc.append(0)  # If denominator is zero, MCC is set to zero
        tpr.append(TPR)
        fpr.append(FPR)
    else:
        MCC = (TP * TN - FP * FN) / math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN))  # MCC formula
        mcc.append(MCC)
        tpr.append(TPR)
        fpr.append(FPR)

    # Create a DataFrame with the results
    df = pd.DataFrame(list(zip(tpr, fpr, mcc)), columns=['TPR', 'FPR', 'MCC'])
    df.to_csv(EVAL_OUT, index=False, sep='\t')  # Save results to a tab-separated file

# Main function to iterate over folds and process evaluation
def main():
    files = parse_args()  # Parse command-line arguments
    for i in range(1, files.K+1):  # Iterate over each fold (1 to K)
        fold = "fold" + str(i)  # Create fold name (e.g., "fold1")
        fold_table = fold + '.tsv'  # Classification table for the fold
        CLASS = os.path.join(files.DIR, fold, fold_table)  # Path to the classification file
        classification = get_class(CLASS)  # Load classification data
        HITS = os.path.join(files.DIR, fold, 'hmm_out/test.out.final')  # Path to the HMM search results
        test_out = get_test_results(HITS)  # Load test results
        EVAL_OUT = os.path.join(files.DIR, fold, 'eval.tsv')  # Path to output evaluation file
        df = calc_rates(classification, test_out, EVAL_OUT)  # Calculate and save performance metrics

# Ensure the main function runs when the script is executed directly
if __name__ == "__main__":
    main()
