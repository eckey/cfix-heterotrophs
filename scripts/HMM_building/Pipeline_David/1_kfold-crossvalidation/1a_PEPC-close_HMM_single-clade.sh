#!/bin/bash -x
#SBATCH --job-name=PEPC-hmm
#SBATCH -n "1"
#SBATCH --mem 2G
#SBATCH -t 2-0:00:00
#SBATCH -o PEPC_HMM_.%j.%N.out
#SBATCH -e PEPC_HMM_.%j.%N.err

# enviroment setup and loading of modules
module load mafft/7.453                 # Load MAFFT for sequence alignment
echo "MAFFT loaded"
module load hmmer/3.3.1                  # Load HMMER for HMM construction
echo "hmmer loaded"
module load Kaleido/0.2.1-GCCcore-13.3.0

source /g/bork3/home/fullam/miniconda3/etc/profile.d/conda.sh
conda activate we

# Parse command-line options for clade
CLADE=""
while getopts 'c:' option; do
    case "${option}" in
        c) CLADE=${OPTARG} ;;  # -g specifies the target clade
        *) echo "Usage: $0 -c <clade_name>" >&2; exit 1 ;;
    esac
done

# Exit if no clade was specified
if [ -z "${CLADE}" ]; then
    echo "Error: Clade name must be specified with -g option." >&2
    exit 1
fi

# Set the base directory for input and output files
dir="/g/scb/bork/eckey/cfix/data/dataprep/01e_HMMs/1_PEPC-close"

# Input path for clade sequences
CLADE_SEQ="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/1_PEPC-close/PEPC_clean_clade-seq.fasta"
DIS_SEQ="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/1_PEPC-close/eval/PEPC-close_discard-seq.fasta"

# Define file paths and variables
INDIR="${dir}/${CLADE}/input"
HMM="${INDIR}/${CLADE}.hmm"
POS_SEQ="${INDIR}/${CLADE}_seq.fasta"
NEG_SEQ="${INDIR}/${CLADE}_neg-seq.fasta"
OUTDIR="${dir}/${CLADE}/output"
NUM_FOLDS=10
mkdir -p "${INDIR}" "${OUTDIR}"

#create ALLSEQ
ALLSEQ="${INDIR}/PC-close_all-seq.fasta"
cat "$CLADE_SEQ" "$DIS_SEQ" > "$ALLSEQ"

# Extract sequences for the specified clade
awk -v clade="${CLADE}" \
    -v pos_out="${POS_SEQ}" \
    -v neg_out="${NEG_SEQ}" '
    /^>/ {
        match($0, /_([^_]+)$/, arr)
        write_to_pos = (arr[1] == clade)
    }
    {
        if (write_to_pos) {
            print > pos_out
        } else {
            print > neg_out
        }
    }
' "${ALLSEQ}"

# Align the positive sequences using MAFFT
mafft-fftns --adjustdirection --thread ${SLURM_NTASKS} "${POS_SEQ}" > "${INDIR}/${CLADE}.mafft-fftns.fasta"

# Build a Hidden Markov Model using the aligned sequences
hmmbuild -n "${CLADE}" --cpu ${SLURM_NTASKS} "${HMM}" "${INDIR}/${CLADE}.mafft-fftns.fasta"

# Perform k-fold cross-validation and process folds using a Python script
python3 - <<EOF
import os
import pandas as pd
from sklearn.model_selection import KFold
from Bio import SeqIO

# Paths and parameters
POS_SEQ = "${POS_SEQ}"
NEG_SEQ = "${NEG_SEQ}"
OUTDIR = "${OUTDIR}"
NUM_FOLDS = int("${NUM_FOLDS}")

# Load FASTA sequences into a DataFrame
def load_fasta(file, sequence_type):
    return pd.DataFrame([{"NAME": record.id, "SEQUENCE": str(record.seq), "TYPE": sequence_type, "CLASS": ""} 
                         for record in SeqIO.parse(file, "fasta")])  

# Create k-fold splits
def kfold_split(data, kfold, sequence_type):
    data["TYPE"] = sequence_type
    data["CLASS"] = ""
    folds = {}
    for i, (train_index, test_index) in enumerate(kfold.split(data)):
        fold = data.copy()
        fold.iloc[train_index, fold.columns.get_loc("CLASS")] = "train"
        fold.iloc[test_index, fold.columns.get_loc("CLASS")] = "test"
        folds[f"fold{i+1}"] = fold
    return folds

# Save folds to output directory
def save_folds(dfs_tp, dfs_fp, OUTDIR, num_folds):
    os.makedirs(OUTDIR, exist_ok=True)
    for i in range(1, num_folds + 1):
        fold_name = f"fold{i}"
        fold_dir = os.path.join(OUTDIR, fold_name)
        os.makedirs(fold_dir, exist_ok=True)

        # Combine true positives and false positives for this fold
        combined = pd.concat([dfs_tp[fold_name], dfs_fp[fold_name]])
        tsv_file = os.path.join(fold_dir, f"{fold_name}.tsv")
        combined.to_csv(tsv_file, index=False, sep='\t')

        # Create train and test FASTA files
        train_fasta = os.path.join(fold_dir, "train.fasta")
        test_fasta = os.path.join(fold_dir, "test.fasta")

        with open(train_fasta, "w") as train_file, open(test_fasta, "w") as test_file:
            for _, row in combined.iterrows():
                fasta_entry = f">{row['NAME']}\n{row['SEQUENCE']}\n"
                if row["CLASS"] == "train":
                    train_file.write(fasta_entry)
                elif row["CLASS"] == "test":
                    test_file.write(fasta_entry)

# Main logic for k-fold cross-validation
tp_data = load_fasta(POS_SEQ, "TP")
fp_data = load_fasta(NEG_SEQ, "FP")
kfold = KFold(n_splits=NUM_FOLDS, random_state=1, shuffle=True)
dfs_tp = kfold_split(tp_data, kfold, "TP")
dfs_fp = kfold_split(fp_data, kfold, "FP")
save_folds(dfs_tp, dfs_fp, OUTDIR, NUM_FOLDS)
EOF

# Process each fold within the same loop for the given clade
for i in $(seq 1 ${NUM_FOLDS}); do

    # Create directories for storing HMM output and profiles
    mkdir -p ${OUTDIR}/fold${i}/hmm_out ${OUTDIR}/fold${i}/profiles

    # Run hmmsearch on the training sequences in the current fold
    hmmsearch \
        --tblout ${OUTDIR}/fold${i}/hmm_out/train.out \
        --noali \
        --cpu ${SLURM_NTASKS} \
        ${HMM} ${OUTDIR}/fold${i}/train.fasta # Input HMM and training sequences

    # Filter the hmmsearch output to retain only useful columns (sequence name and score)
    grep -v '^#' ${OUTDIR}/fold${i}/hmm_out/train.out | \
    awk '{print $1"\t"$6}' > ${OUTDIR}/fold${i}/hmm_out/train.out.final # Extract sequence name and score

    # Copy the HMM profile into the profiles directory for the current fold
    cp ${HMM} ${OUTDIR}/fold${i}/profiles

    # Rename the copied HMM file to a standard name (profile.hmm)
    mv "${OUTDIR}/fold${i}/profiles/${CLADE}.hmm" "${OUTDIR}/fold${i}/profiles/profile.hmm"
done

# Run the Python script for cross-validation and GA thresholding
python /g/scb/bork/eckey/cfix/scripts/HMM_building/Pipeline_David/1_kfold-crossvalidation/1b_kfold_cross_GA.py \
     -k ${NUM_FOLDS} \
     --dir ${OUTDIR}

wait

# Process test sequences for each fold
for i in $(seq 1 ${NUM_FOLDS}); do
    # Run HMM search on test sequences for the current fold
    hmmsearch \
        --tblout ${OUTDIR}/fold${i}/hmm_out/test.out \
        --cut_ga --noali --cpu ${SLURM_NTASKS} \
        ${OUTDIR}/fold${i}/profiles/profile.thresh.hmm ${OUTDIR}/fold${i}/test.fasta

    # Clean and format the HMM search results
    grep -v '^#' ${OUTDIR}/fold${i}/hmm_out/test.out | \
    awk '{print $1"\t"$6}' > ${OUTDIR}/fold${i}/hmm_out/test.out.final
done

# Run the Python script for model evaluation
python /g/scb/bork/eckey/cfix/scripts/HMM_building/Pipeline_David/1_kfold-crossvalidation/1c_kfold_model-eval.py \
     -k ${NUM_FOLDS} \
     --dir ${OUTDIR}
