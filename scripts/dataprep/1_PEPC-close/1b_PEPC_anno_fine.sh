#!/bin/bash
# Define job name
#SBATCH --job-name=PEPCanno

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 0-01:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01b_KO_IDs_map/1_PEPC-close"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_validation-trees/1_PEPC-close"

# Define the name of the output annotation file
annotation_file="${output_dir}/PEPC_anno_fine.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01958.fasta" # PC
    "${input_dir}/K01959.fasta" # PC a
    "${input_dir}/K01960.fasta" # PC b
    "${input_dir}/K01965.fasta" # Prop-CoA a
    "${input_dir}/K01966.fasta" # Prop-CoA b
    "${input_dir}/K01595.fasta" # PEP-C 
    "${input_dir}/K25932.fasta" # PPPC a
    "${input_dir}/K25933.fasta" # PPPC b
    "${input_dir}/K25934.fasta" # PPPC g
    "${input_dir}/K25935.fasta" # PPPC d
    "${input_dir}/K10854.fasta" # AC a
    "${input_dir}/K10855.fasta" # AC b
    "${input_dir}/K10856.fasta" # AC g
    "${input_dir}/K10701.fasta" # APC
    )

# Define the colors for each FASTA file
colors=(
    "#d62929"  # Color for PC           50% red
    "#eb9494"  # Color for PC a         65%
    "#f3bfbf"  # Color for PC b         80%
    "#bf8040"  # Color for Prop-CoA a   50% brown
    "#d2a679"  # Color for Prop-CoA b   65%
    "#80d629"  # Color for PEPC        50% green
    "#29d6d6"  # Color for PPPC a       50% light blue
    "#1d9696"  # Color for PPPC b       35%
    "#69e2e2"  # Color for PPPC g       65%
    "#a9efef"  # Color for PPPC d       80%
    "#d6d629"  # Color for AC a         50% yellow
    "#e2e269"  # Color for AC b         65%
    "#efefa9"  # Color for AC g         80%
    "#9900ff"  # Color for APC g         80% violett
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


