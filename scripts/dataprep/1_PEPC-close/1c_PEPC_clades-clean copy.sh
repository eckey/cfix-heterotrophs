#!/bin/bash
# Define job name
#SBATCH --job-name=PCclades

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 1-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd  

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/2_PC-close"
input_fasta="PC-close.fasta"
output_file="PC_clean_clade-seq.fasta"
discarded_file="PC-close_discard-seq.fasta"
eval_file="PC-close_clade-coverage_report.txt"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_fasta} .
cp ${input_dir}/*.txt .

# Temporary file to store results
temp_output=$(mktemp)

# Loop through all .txt files in the directory
for txt_file in ${scratch_dir}/*.txt; do
    # Extract the base name of the txt file for the label
    base_name=$(basename "$txt_file" .txt)
    label="${base_name}"

    # Read the entire fasta file content once into memory (use a variable or file depending on size)
    fasta_content=$(<"$input_fasta")

    # Loop through each line of the current .txt file
    while IFS= read -r header; do
        # Sanitize the header
        sanitized_header=$(echo "$header" | tr -d '\r' | sed 's/^[ \t]*//;s/[ \t]*$//')

        # Search for the header and extract the sequence until the next ">"
        sequence=$(echo "$fasta_content" | sed -n "/^>${sanitized_header}/,/^>/p" | sed '1d')

        # If a sequence is found, modify the header and append to the temporary output file
        if [ -n "$sequence" ]; then
            new_header=">${sanitized_header}_${label}"
            echo "$new_header" >> "$temp_output"
            echo "$sequence" >> "$temp_output"
        fi
    done < "$txt_file"
done

# Move the results to the output file
mv "$temp_output" "$output_file"

echo "PC_clean_clade-seq.fasta created"

# Declare an associative array to track found headers
declare -A found_headers

# Create the discarded file
while IFS= read -r original_header; do
    # Check if the header has already been found
    if [[ -z "${found_headers[$original_header]}" ]]; then
        # Mark the header as found
        found_headers["$original_header"]=1

        # Extract the sequence for the discarded header
        sequence=$(grep -A 1000 -F ">${original_header}" "$input_fasta" | awk '/^>/ && NR > 1 {exit} {print}')

        if [ -n "$sequence" ]; then
            # Modify the header to indicate it is discarded
            new_header=">${original_header}_discarded"
            # Write the sequence block to the discarded file
            echo "$sequence" | sed "1s/^>.*/$new_header/" >> "$discarded_file"
        fi
    fi
done < <(awk '/^>/{print substr($0, 2)}' "$input_fasta")

echo "Discarded fasta file created"

# Generate the evaluation report
{
    # Total sequences in the original fasta file
    total_sequences=$(grep -c "^>" "$input_fasta")

    # Clade sequences in the output file
    clade_sequences=$(grep -c "^>" "$output_file")

    # Calculate tree coverage
    tree_coverage=$(echo "scale=2; ($clade_sequences / $total_sequences) * 100" | bc)

    # Print the summary
    echo "Total_tree_sequences: $total_sequences"
    echo "Clade_sequences: $clade_sequences"
    echo "tree_coverage: $tree_coverage%"

    # List each clade and its sequence count
    for txt_file in "$input_dir"/*.txt; do
        base_name=$(basename "$txt_file" .txt)
        clade_count=$(grep -c "_${base_name}" "$output_file")
        echo "$base_name: $clade_count"
    done
} > "$eval_file"

echo "Evaluation report created" 

# Copy output files back to own directory
cp ${scratch_dir}/${output_file} ${input_dir}/
cp ${scratch_dir}/${discarded_file} ${input_dir}/
cp ${scratch_dir}/${eval_file} ${input_dir}/

# Remove scratch directory 
rm -rf ${scratch_dir}