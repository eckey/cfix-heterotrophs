#!/bin/bash
# Define job name
#SBATCH --job-name=PEPCclade

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 1-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd  

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/1_PEPC-close"
input_fasta="${input_dir}/PEPC-close.fasta"
output_file="${input_dir}/PEPC_clean_clade-seq.fasta"

# Loop through all .txt files in the directory
for txt_file in "$input_dir"/*.txt; do

    # Define base name 
    base_name=$(basename "$txt_file" .txt)

    # Cut base name before first underscore and after second
    label=$(echo "$base_name")

    # Loop through each line of the current .txt file
    while read -r header; do

    # Sanitize the header to ensure compatibility
        header=$(echo "$header" | tr -d '\r' | sed 's/^[ \t]*//;s/[ \t]*$//')
    
     # Search the fasta file for the sanitized header and extract the sequence block until the next ">"
        sequence=$(grep -A 1000 -F ">${header}" "$input_fasta" | awk '/^>/ && NR > 1 {exit} {print}')

    # Check if a match was found (non-empty sequence)
        if [ -n "$sequence" ]; then

            # Modify the header to include the new label
            new_header=">${header}_${label}"

            # Replace the first header line and append the sequence block to the output file
            echo "$sequence" | sed "1s/^>.*$/$new_header/" >> "$output_file"
        fi 

    done < "$txt_file"
done