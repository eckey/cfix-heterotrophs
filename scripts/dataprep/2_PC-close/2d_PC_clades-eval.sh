#!/bin/bash -x
# Define job name
#SBATCH --job-name=PEPCeval

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 1-00:00:00

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_clean-up/2_PC-close"
output_dir="${input_dir}/eval"
tree_fasta="/g/scb/bork/eckey/cfix/data/dataprep/01b_KO_IDs_map/2_PC-close/PC-close.fasta"
clade_fasta="${input_dir}/PC_clean_clade-seq.fasta"
discarded_fasta="${output_dir}/PC-close_discard-seq.fasta"
eval_file="${output_dir}/PC-close_clade-eval.txt"
clade_ids="${output_dir}/PC-close_clade-ids.txt"

# Create the output directory if it doesn't exist
mkdir -p "$output_dir"

# Extract headers from clade fasta and strip everything after and including the last underscore
grep "^>" "$clade_fasta" | sed "s/^>//; s/_\([^_]*\)$//" > "$clade_ids"

# Filter PEPC-close.fasta to get sequences that are NOT in the clade list
awk 'BEGIN {
    while((getline < "'$clade_ids'") > 0) clade[$1]=1
    in_sequence = 0
} 
/^>/ { 
    header = substr($0, 2)  # Extract the header name (without ">" at the beginning)
    in_sequence = !(header in clade)  # Check if the header is NOT in clade
    if (in_sequence) {
        print $0 "_discarded" # Print the header to the output file if the sequence is to be kept
    }
    next
} 
in_sequence { 
    print $0  # Print the sequence lines for the current sequence
}' "$tree_fasta" > "$discarded_fasta"

echo "Discarded fasta file created"

# Generate the evaluation report
{
    # Total sequences in the original fasta file
    total_sequences=$(grep -c "^>" "$tree_fasta")

    # Clade sequences in the output file
    clade_sequences=$(grep -c "^>" "$clade_fasta")

    # Calculate tree coverage
    tree_coverage=$(echo "scale=2; ($clade_sequences / $total_sequences) * 100" | bc)

    discarded_sequences=$(grep -c "^>" "$discarded_fasta")

    # Print the summary
    echo "Total_tree_sequences: $total_sequences"
    echo "Clade_sequences: $clade_sequences"
    echo "tree_coverage: $tree_coverage%"
    echo "discarded_sequences: $discarded_sequences"

    # List each clade and its sequence count
    for txt_file in "$input_dir"/*.txt; do
        base_name=$(basename "$txt_file" .txt)
        clade_count=$(grep -c "_${base_name}" "$clade_fasta")
        echo "$base_name: $clade_count"
    done
} > "$eval_file"

echo "Evaluation file created"
