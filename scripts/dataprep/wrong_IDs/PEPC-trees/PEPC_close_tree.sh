#!/bin/bash
# Define job name
#SBATCH --job-name=PEPC-close

# Define number of cores to use
#SBATCH -n 16

# Define memory amount (3x cores in GB)
#SBATCH --mem 48G

# Define maximum run time
#SBATCH -t 14-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.test_tree.out

# Load modules
module load mafft/7.453
module load iqtree

# File and directory variables
input_file="PEPC_com_close.fasta"
aligned_file="PEPC_com_close.mafft-fftns.fasta"
output_prefix="PEPC_com_iqtree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PEPC_close"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PEPC"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 12 --anysymbol ${input_file} > ${aligned_file}

# Annex duplicate sequences, create temp file, and overwrite original
awk '(/^>/ && s[$0]++){$0=$0"_"s[$0]}1;' ${aligned_file} > tmp && mv tmp ${aligned_file}

# Run phylogenetic tree construction 
iqtree -s ${aligned_file} -m LG+I+G -B 1000 -alrt 1000 -nt AUTO -ntmax 12 --prefix ${output_prefix} -cptime 1800 -nm 200

# Copy output files back to own directory
cp ${aligned_file} ${output_prefix}* ${output_dir}

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
