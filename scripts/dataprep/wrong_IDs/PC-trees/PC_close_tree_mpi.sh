#!/bin/bash
# Define job name
#SBATCH --job-name=PC-mpi

# Partition name
#SBATCH --partition=bigmem   # Job needs at least 140GB memory

# Set number of MPi tasks
#SBATCH --ntasks=4

# Define number of cores per task
#SBATCH --cpus-per-task=8

# Define memory amount (24x cores in GB)
#SBATCH --mem 160G

# Define maximum run time
#SBATCH -t 30-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.PC_close.out
#SBATCH -e slurm.%j.%N.PC_close.err

# Load modules
module load mafft/7.453
module load OpenMPI/4.1.6-GCC-13.2.0
module load iqtree
module load IQ-TREE/2.2.1-gompi-2021b

# File and directory variables
input_file="PC_close_com.fasta"
aligned_file="PC_close_com.mafft-fftns.fasta"
output_prefix="PC_com_iqtree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC_close"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PC_close"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Run multiple sequence alignment
mafft-fftns --adjustdirection --thread ${SLURM_CPUS_PER_TASK} --anysymbol ${input_file} > ${aligned_file}

# Annex duplicate sequences, create temp file, and overwrite original
awk '(/^>/ && s[$0]++){$0=$0"_"s[$0]}1;' ${aligned_file} > tmp && mv tmp ${aligned_file}
iqtree-mpi
# Run phylogenetic tree construction with parallelization using MPI and OpenMP threads
mpirun -np ${SLURM_NTASKS} --bind-to none iqtree-mpi -s ${aligned_file} -m LG+I+G \
    -bb 1000 -alrt 1000 --prefix ${output_prefix} -cptime 1800 -nm 200 \
    -nt ${SLURM_CPUS_PER_TASK}  # Pass the number of OpenMP threads to IQ-TREE

# Copy output files back to own directory
cp ${aligned_file} ${output_prefix}* ${output_dir}

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
