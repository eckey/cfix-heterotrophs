#!/bin/bash
# Definde job name
#SBATCH --job-name=test_tree

# Define number of cores to use
#SBATCH -n 18

# Define memory amount (4x cores in GB)
#SBATCH --mem 72G

# Define maximum run time
#SBATCH -t 2-00:00:00

#Redirect standard output 
#SBATCH -o slurm.%j.%N.test_tree.out

# load modules
module load mafft/7.453
module load iqtree

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Setting directory path 
dir="/g/scb/bork/eckey/cfix/data/dataprep"

# Changeing to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${dir}/01c_KO_IDs_map/pyruvate-carboxylase/pyruvate-carboxylase_com3.fasta .

# Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 12 --anysymbol pyruvate-carboxylase_com3.fasta > pc_com.mafft-fftns.fasta

# annex duplicate sequences, create temp file and overwrite original
awk '(/^>/ && s[$0]++){$0=$0"_"s[$0]}1;' pc_com.mafft-fftns.fasta  > tmp && mv tmp pc_com.mafft-fftns.fasta

# Run phylogenetic tree construction 
iqtree -s pc_com.mafft-fftns.fasta -m LG+I+G -B 1000 -alrt 1000 -nt AUTO -ntmax 12 --prefix pc_com_iqtree -cptime 1800 -nm 200

# Copy output files back to own directory
cp pc_com.mafft-fftns.fasta pc_com_iqtree* ${dir}/01d_validation-trees/pyruvate-carboxylase3

# return to home directory
cd ~

# Removes scratch directory 
rm -rf ${scratch_dir}