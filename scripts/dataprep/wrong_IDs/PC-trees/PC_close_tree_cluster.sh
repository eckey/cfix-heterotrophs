#!/bin/bash
# Define job name
#SBATCH --job-name=PC-cluster

# Partition name
#SBATCH --partition=bigmem   # Job needs at least 103GB memory

# Define number of cores to use
#SBATCH -n 8

# Define memory amount 
#SBATCH --mem 128G

# Define maximum run time
#SBATCH -t 30-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.PC_close.out
#SBATCH -e slurm.%j.%N.PC_close.err

# Load modules
module load mafft/7.453
module load iqtree
module load CD-HIT/4.8.1-GCC-11.2.0

# File and directory variables
input_file="PC_close_com.fasta"
cdhit_output="PC_close_com.cdhit.fasta"
aligned_file="PC_close_com.mafft-fftns.fasta"
output_prefix="PC_com_iqtree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC_close"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PC_close"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Step 1: Precluster sequences with CD-HIT
cd-hit -i ${input_file} -o ${cdhit_output} -c 0.95 -n 5 -T 8 -M 32000

# Step2: Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 8 --anysymbol ${cdhit_output} > ${aligned_file}

# Step 3: annex duplicate sequences, create temp file, and overwrite original
# awk '(/^>/ && s[$0]++){$0=$0"_"s[$0]}1;' ${aligned_file} > tmp && mv tmp ${aligned_file}

# Step 4: Run phylogenetic tree construction 
iqtree -s ${aligned_file} -m LG+I+G -bb 1000 -alrt 1000 -nt AUTO -ntmax 8 --prefix ${output_prefix} -cptime 1800 -nm 200

# Copy output files back to own directory
cp ${aligned_file} ${output_prefix}* ${output_dir}

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
