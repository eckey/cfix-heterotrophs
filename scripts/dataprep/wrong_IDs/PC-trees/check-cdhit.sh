#!/bin/bash
# Define job name
#SBATCH --job-name=cdhit

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 2-00:00:00

module load CD-HIT/4.8.1-GCC-11.2.0
module load mafft/7.453

output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC_close"
input_file="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PC_close/PC_close_com.fasta"
cdhit_output="PC_close_com.cdhit.fasta"
aligned_file="PC_close_com.mafft-fftns.fasta"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file into scratch directory
cp ${input_file} .

# Run cd-hit using the input file in the scratch directory
cd-hit -i $(basename ${input_file}) -o ${cdhit_output} -c 0.99 -n 5 -T $SLURM_NTASKS -M 4000 -d 0

# Step 2: Run multiple sequence alignment
mafft-fftns --adjustdirection --thread $SLURM_NTASKS --anysymbol ${cdhit_output} > ${aligned_file}

# Copy output files back to the specified output directory
cp ${cdhit_output} ${aligned_file} ${output_dir}

# Remove scratch directory
rm -rf ${scratch_dir}
