#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=anno_3

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (4x cores in GB)
#SBATCH --mem 1G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                              # Run the job from the current directory

         


# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/seq_pos"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/pyruvate-carboxylase3"

# Define the name of the output annotation file
annotation_file="${output_dir}/pc_com_itol_anno3-3.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01958_pos.fasta" # PC
    "${input_dir}/K01959_pos.fasta" # PC a
    "${input_dir}/K01960_pos.fasta" # PC b
    "${input_dir}/K01965_pos.fasta" # Prop-CoA a
    "${input_dir}/K01966_pos.fasta" # Prop-CoA b
    )

# Define the colors for each FASTA file
colors=(
    "#B71C1C"  # Color for PC
    "#D32F2F"  # Color for PC a
    "#E53935"  # Color for PC b
    "#757575"  # Color for prop-CoA a
    "#BDBDBD"  # Color for prop-CoA b
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


