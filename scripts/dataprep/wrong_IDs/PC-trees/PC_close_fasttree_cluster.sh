#!/bin/bash
# Define job name
#SBATCH --job-name=PC-cluster

# Define number of cores to use
#SBATCH -n 8

# Define memory amount 
#SBATCH --mem 16G

# Define maximum run time
#SBATCH -t 14-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.PC_fast.out
#SBATCH -e slurm.%j.%N.PC_fast.err

# Load modules
module load CD-HIT/4.8.1-GCC-11.2.0
module load mafft/7.453
module load FastTree/2.1.11-GCCcore-11.2.0


# File and directory variables
input_file="PC_close_com.fasta"
cdhit_output="PC_close_com.cdhit.fasta"
aligned_file="PC_close_com.mafft-fftns.fasta"
output_file="PC_com_fasttree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC_close"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PC_close"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Step 1: Precluster sequences with CD-HIT
cd-hit -i ${input_file} -o ${cdhit_output} -c 0.99 -n 5 -T 8 -M 16000 -d 0

# Step 2: Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 8 --anysymbol ${cdhit_output} > ${aligned_file}

# Step 3: Run phylogenetic tree construction 
FastTree -lg -gamma -n 100 ${aligned_file} > ${output_file}

# Copy output files back to own directory
cp ${scratch_dir}/* ${output_dir}/

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
