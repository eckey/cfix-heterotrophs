#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PC_anno

# Define number of cores to use
#SBATCH -n 8

# Define memory amount (1x cores in GB)
#SBATCH --mem 8G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/PC_close"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC_close"

# Define the name of the output annotation file
annotation_file="${output_dir}/PC_close_anno1_fine.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01958_pos.fasta" # PC                1
    "${input_dir}/K01959_pos.fasta" # PC a              2
    "${input_dir}/K01960_pos.fasta" # PC b              3
    "${input_dir}/K11262_neg.fasta" # ACC 1             4
    "${input_dir}/K01964_neg.fasta" # ACC 2             5
    "${input_dir}/K01961_neg.fasta" # ACC biotin        6
    "${input_dir}/K01962_neg.fasta" # ACC transf a      7   
    "${input_dir}/K01963_neg.fasta" # ACC transf b      8
    "${input_dir}/K11263_neg.fasta" # ACC carrier       9
    "${input_dir}/K01965_neg.fasta" # Prop-CoA a        10
    "${input_dir}/K01966_neg.fasta" # Prop-CoA b        11
    "${input_dir}/K01968_neg.fasta" # MCC a             12
    "${input_dir}/K01969_neg.fasta" # MCC b             13
    "${input_dir}/K13777_neg.fasta" # GCC a             14
    "${input_dir}/K13778_neg.fasta" # GCC b             15
    "${input_dir}/K01964_neg.fasta" # ACC-PCC           16
    "${input_dir}/K15036_neg.fasta" # ACC-PCC           17
    "${input_dir}/K15052_neg.fasta" # ACC-PCC           18
    "${input_dir}/K18472_neg.fasta" # ACC-PCC transf    19
    "${input_dir}/K18603_neg.fasta" # ACC-PCC           20
    "${input_dir}/K18604_neg.fasta" # ACC-PCC           21
    "${input_dir}/K19312_neg.fasta" # ACC-PCC transf    22
    "${input_dir}/K22568_neg.fasta" # ACC-PCC           23
    "${input_dir}/K20140_neg.fasta" # OC large          24
    "${input_dir}/K20141_neg.fasta" # OC small          25
    )


# Define the colors for each FASTA file
colors=(
    "#802000"  # Color for PC           red/orange      1  25%
    "#cc3300"  # Color for PC a                         2  40%
    "#ff531a"  # Color for PC b                         3  55%
    "#666600"  # Color for ACC 1        yellow          4  20%
    "#b3b300"  # Color for ACC 2                        5  35%
    "#e6e600"  # Color for ACC biotin                   6  45%
    "#ffff1a"  # Color for ACC tarsnf b                 7  55%
    "#ffff4d"  # Color for ACC transf a                 8  65%
    "#ffff80"  # Color for ACC carrier                  9  75%
    "#009926"  # Color for Prop-CoA a   green           10 30%
    "#00e639"  # Color for Prop-CoA b                   11 45%
    "#004d99"  # Color for MCC a        blue            12 30%
    "#0073e6"  # Color for MCC b                        13 45%
    "#4d0099"  # Color for GCC a        violett         14 30%
    "#7300e6"  # Color for GCC b                        15 45%
    "#330033"  # Color for ACC-PCC      pink            16 10%     K01964
    "#660066"  # Color for ACC-PCC                      17 20%      K15036
    "#990099"  # Color for ACC-PCC                      18 30%     K15052
    "#cc00cc"  # Color for ACC-PCC                      19 40%     K18472
    "#ff00ff"  # Color for ACC-PCC                      20 50%     K18603
    "#ff33ff"  # Color for ACC-PCC                      21 60%     K18604
    "#ff66ff"  # Color for ACC-PCC                      22 70%     K19321
    "#ff99ff"  # Color for ACC-PCC                      23 80%     K22568
    "#928787"  # Color for OC large     grey            24 55%
    "#b6afaf"  # Color for OC small                     25 70%
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


