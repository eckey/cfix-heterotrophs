#!/bin/bash
# Define job name
#SBATCH --job-name=ACPCAP

# Partition name
#SBATCH --partition=bigmem   

# Define number of cores to use
#SBATCH -n 8

# Define memory amount 
#SBATCH --mem 64G

# Define maximum run time
#SBATCH -t 30-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.AP_clust.out
#SBATCH -e slurm.%j.%N.AP_clust.err

# Load modules
module load mafft/7.453
module load iqtree
module load CD-HIT/4.8.1-GCC-11.2.0

# File and directory variables
input_file="ACC_PCC_ACC-PCC_com.fasta"
cdhit_output="AC_PC_AP_com.cdhit.fasta"
aligned_file="AC_PC_AP_com.mafft-fftns.fasta"
output_prefix="AC_PC_AP_com_iqtree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/ACC_PCC_ACC-PCC"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/ACC_PCC_ACC-PCC"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Step 1: Precluster sequences with CD-HIT
cd-hit -i ${input_file} -o ${cdhit_output} -c 0.90 -n 5 -T 8 -M 64000 -d 0

# Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 8 --anysymbol ${cdhit_output} > ${aligned_file}


# Run phylogenetic tree construction 
iqtree -s ${aligned_file} -m LG+I+G -B 1000 -alrt 1000 -nt 8 --prefix ${output_prefix} -cptime 1800 -nm 200

# Copy output files back to own directory
cp ${cdhit_output} ${aligned_file} ${output_prefix}* ${output_dir} 

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
