#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PC_anno

# Define number of cores to use
#SBATCH -n 8

# Define memory amount (1x cores in GB)
#SBATCH --mem 8G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/ACC_PCC_ACC-PCC"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/ACC_PCC_ACC-PCC"

# Define the name of the output annotation file
annotation_file="${output_dir}/ACC_PCC_ACC-PCC_anno1_fine.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01946.fasta" # ACC 2 
    "${input_dir}/K11262.fasta" # ACC 1                        
    "${input_dir}/K01961.fasta" # ACC biotin        
    "${input_dir}/K01962.fasta" # ACC transf a      
    "${input_dir}/K01963.fasta" # ACC transf b      
    "${input_dir}/K11263.fasta" # ACC carrier       
    "${input_dir}/K01965.fasta" # Prop-CoA a        
    "${input_dir}/K01966.fasta" # Prop-CoA b       
    "${input_dir}/K01964.fasta" # ACC-PCC          
    "${input_dir}/K15036.fasta" # ACC-PCC           
    "${input_dir}/K15052.fasta" # ACC-PCC           
    "${input_dir}/K18472.fasta" # ACC-PCC transf    
    "${input_dir}/K18603.fasta" # ACC-PCC           
    "${input_dir}/K18604.fasta" # ACC-PCC           
    "${input_dir}/K19312.fasta" # ACC-PCC transf    
    "${input_dir}/K22568.fasta" # ACC-PCC           
    )


# Define the colors for each FASTA file
colors=(

    "#666600"  # Color for ACC 1        yellow          4  20%
    "#b3b300"  # Color for ACC 2                        5  35%
    "#e6e600"  # Color for ACC biotin                   6  45%
    "#ffff1a"  # Color for ACC tarsnf b                 7  55%
    "#ffff4d"  # Color for ACC transf a                 8  65%
    "#ffff80"  # Color for ACC carrier                  9  75%
    "#009926"  # Color for Prop-CoA a   green           10 30%
    "#00e639"  # Color for Prop-CoA b                   11 45%
    "#330033"  # Color for ACC-PCC      pink            16 10%     K01964
    "#660066"  # Color for ACC-PCC                      17 20%      K15036
    "#990099"  # Color for ACC-PCC                      18 30%     K15052
    "#cc00cc"  # Color for ACC-PCC                      19 40%     K18472
    "#ff00ff"  # Color for ACC-PCC                      20 50%     K18603
    "#ff33ff"  # Color for ACC-PCC                      21 60%     K18604
    "#ff66ff"  # Color for ACC-PCC                      22 70%     K19321
    "#ff99ff"  # Color for ACC-PCC                      23 80%     K22568
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


