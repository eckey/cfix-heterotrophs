#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=anno_4

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (4x cores in GB)
#SBATCH --mem 1G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                              # Run the job from the current directory



# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/pyruvate-carboxylase/attempt-4"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/pyruvate-carboxylase4"

# Define the name of the output annotation file
annotation_file="${output_dir}/pc_com_itol_anno4-2.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01958_pos.fasta" # PC
    "${input_dir}/K01959_pos.fasta" # PC a
    "${input_dir}/K01960_pos.fasta" # PC b
    "${input_dir}/K01965_neg.fasta" # Prop-CoA a
    "${input_dir}/K01966_neg.fasta" # Prop-CoA b
    "${input_dir}/K01968_neg.fasta" # Metcro-CoA a
    "${input_dir}/K01969_neg.fasta" # Metcro-CoA b
    "${input_dir}/K01595_swiss_neg.fasta" # PEP-C 
    )

# Define the colors for each FASTA file
colors=(
    "#7C2529"  # Color for PC
    "#D22630"  # Color for PC a
    "#F8485E"  # Color for PC b
    "#007FA3"  # Color for Prop-CoA a
    "#00A9CE"  # Color for Prop-CoA b
    "#84BD00"  # Color for Metcro-CoA a
    "#C5E86C"  # Color for Metcro-CoA b
    "#AD1ACC"  # Color for # PEP-C 
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


