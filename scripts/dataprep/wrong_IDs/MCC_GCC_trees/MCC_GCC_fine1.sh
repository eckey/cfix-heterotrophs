#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PC_anno

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/MCC_GCC"
output_dir="/g/scb/bork/eckey/cfix/scripts/dataprep/MCC_GCC_trees"

# Define the name of the output annotation file
annotation_file="${output_dir}/MCC_GCC_fine1.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01961.fasta" # ACC biotin caroxylase control 
    "${input_dir}/K01968.fasta" # MCC alpha
    "${input_dir}/K01969.fasta" # MCC beta
    "${input_dir}/K13777.fasta" # GCC alpha
    "${input_dir}/K13778.fasta" # GCC beta 
    )
# Define the colors for each FASTA file
colors=(
    "#d62929"  # ACC biotin caroxylase control           50%
    "#d62980"  # MCC alpha   50%
    "#e269a6"  # MCC beta   65%
    "#d6d629"  # GCC alpha    50%
    "#e2e269"  # GCC beta     65%  
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


