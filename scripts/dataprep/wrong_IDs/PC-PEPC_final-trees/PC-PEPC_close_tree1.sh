#!/bin/bash
# Define job name
#SBATCH --job-name=PC-PEPC_close

# Define number of cores to use
#SBATCH -n 16

# Define memory amount (3x cores in GB)
#SBATCH --mem 48G

# Define maximum run time
#SBATCH -t 14-00:00:00

# Redirect standard output 
#SBATCH -o slurm.%j.%N.test_tree.out

# Load modules
module load mafft/7.453
module load iqtree

# File and directory variables
input_file="PC-PEPC_close_com.fasta"
aligned_file="PC-PEPC_com.mafft-fftns.fasta"
output_prefix="PC-PEPC_com_iqtree"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC-PEPC_close/PC-PEPC_close1"
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/pyruvate-carboxylase/PC-PEPC_close/PC-PEPC_close1"

# Create scratch output directory with username and Job ID
scratch_dir="/scratch/${USER}/${SLURM_JOB_ID}"
mkdir -p ${scratch_dir}

# Change to scratch directory
cd ${scratch_dir}

# Copy input file
cp ${input_dir}/${input_file} .

# Run multiple sequence alignment
mafft-fftns --adjustdirection --thread 12 --anysymbol ${input_file} > ${aligned_file}

# Annex duplicate sequences, create temp file, and overwrite original
awk '(/^>/ && s[$0]++){$0=$0"_"s[$0]}1;' ${aligned_file} > tmp && mv tmp ${aligned_file}

# Run phylogenetic tree construction 
iqtree -s ${aligned_file} -m LG+I+G -B 100 -alrt 1000 -nt AUTO -ntmax 12 --prefix ${output_prefix} -cptime 1800 -nm 200

# Copy output files back to own directory
cp ${aligned_file} ${output_prefix}* ${output_dir}

# Return to home directory
cd ~

# Remove scratch directory 
rm -rf ${scratch_dir}
