#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PC-PEPC_clean

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd  

#SBATCH -o slurm.%j.%N.PC-PEPC_cleanup.out
#SBATCH -e slurm.%j.%N.PC-PEPC_cleanup.err

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01e_clean-up/PC-PEPC_close2.1bb_clean"

# Define name of the input fasta file
input_fasta="${input_dir}/PC-PEPC_close2_com.fasta"

# Define the name of the output fasta
output_file="${input_dir}/PC-PEPC2.1_clean_clade-seq.fasta"

# Loop through all .txt files in the specified directory
for txt_file in "$input_dir"/*.txt; do
    
    # Define base name 
    base_name=$(basename "$txt_file" .txt)

    # Extract the label from the base name (part between first and second underscore)
    label=$(echo "$base_name" | cut -d'_' -f2)

    # DEBUG: Log the file being processed
    echo "Processing file: $txt_file (label: $label)" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out

    # Loop through each line of the current .txt file
    while read -r header; do
        <

        # Log the sanitized header for debugging
        echo "  Sanitized header for grep: '>$header'" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out

        # Search the fasta file for the sanitized header and extract the sequence block
        sequence=$(grep -A 1000 -F ">${header}" "$input_fasta" | awk '/^>/ && NR > 1 {exit} {print}')

        # Log the result of the grep
        if [ -n "$sequence" ]; then
            echo "    Match found for header: $header" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out
        else
            echo "    No match found for header: $header" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out
        fi

        # Check if a match was found (non-empty sequence)
        if [ -n "$sequence" ]; then

            # Remove the last 4 characters from the header (_pos or _neg)
            header="${header%????}"

            # Modify the header to include the new label
            new_header=">${header}_${label}"

            # Log the new header
            echo "    Modified header: $new_header" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out

            # Replace the first header line and append the sequence block to the output file
            echo "$sequence" | sed "1s/^>.*$/$new_header/" >> "$output_file"

            # Log successful appending
            echo "    Appended sequence for header: $new_header" >> slurm.${SLURM_JOB_ID}.${SLURM_JOB_NODELIST}.PC-PEPC_cleanup.out
        fi

    done < "$txt_file"  # End of 'while' loop

done  # End of 'for' loop