#!/bin/bash

#!/bin/bash
# Define job name
#SBATCH --job-name=PC-PEPC_anno

# Define number of cores to use
#SBATCH -n 4

# Define memory amount (1x cores in GB)
#SBATCH --mem 4G

# Define maximum run time
#SBATCH -t 2-00:00:00

# Set the working directory to where the job was submitted from
#$ -cwd                             

# Setting directory path for input and output 
input_dir="/g/scb/bork/eckey/cfix/data/dataprep/01c_KO_IDs_map/pyruvate-carboxylase/PC-PEPC_close"
output_dir="/g/scb/bork/eckey/cfix/data/dataprep/01d_validation-trees/PC-PEPC_close"

# Define the name of the output annotation file
annotation_file="${output_dir}/PC-PEPC_anno_fine1.txt"

# Define the file paths for the FASTA files
fasta_files=(
    "${input_dir}/K01958_pos.fasta" # PC
    "${input_dir}/K01959_pos.fasta" # PC a
    "${input_dir}/K01960_pos.fasta" # PC b
    "${input_dir}/K01965_neg.fasta" # Prop-CoA a
    "${input_dir}/K01966_neg.fasta" # Prop-CoA b
    "${input_dir}/K20140_neg.fasta" # OC large
    "${input_dir}/K20141_neg.fasta" # OC small
    "${input_dir}/K01595_pos.fasta" # PEP-C 
    "${input_dir}/K01595_swiss_neg.fasta" # PEP-C swiss
    "${input_dir}/K25932_neg.fasta" # PPPC a
    "${input_dir}/K25933_neg.fasta" # PPPC b
    "${input_dir}/K25934_neg.fasta" # PPPC g
    "${input_dir}/K25935_neg.fasta" # PPPC d
    "${input_dir}/K10854_neg.fasta" # AC a
    "${input_dir}/K10855_neg.fasta" # AC b
    "${input_dir}/K10856_neg.fasta" # AC g
    "${input_dir}/K10701_neg.fasta" # APC
    )

# Define the colors for each FASTA file
colors=(
    "#d62929"  # Color for PC           50%
    "#eb9494"  # Color for PC a         65%
    "#f3bfbf"  # Color for PC b         80%
    "#d62980"  # Color for Prop-CoA a   50%
    "#e269a6"  # Color for Prop-CoA b   65%
    "#d6d629"  # Color for OC large     50%
    "#e2e269"  # Color for OC small     65%
    "#80d629"  # Color for PEP-C        50%
    "#59961d"  # Color for PEP-C swiss  35%
    "#29d6d6"  # Color for PPPC a       50%
    "#1d9696"  # Color for PPPC b       35%
    "#69e2e2"  # Color for PPPC g       65%
    "#a9efef"  # Color for PPPC d       80%
    "#2980d6"  # Color for AC a         50%
    "#69a6e2"  # Color for AC b         65%
    "#a9ccef"  # Color for AC g         80%
    "#8029d6"  # Color for APC          50%
)


# Initialize the iTOL annotation file with headers
echo -e "TREE_COLORS\nSEPARATOR TAB\nDATA" > "$annotation_file"

# Loop through each FASTA file and its corresponding color
for i in "${!fasta_files[@]}"; do
    fasta_file="${fasta_files[$i]}"
    chosen_color="${colors[$i]}"
    
    # Extract headers and append them to the annotation file with the chosen color
    grep "^>" "$fasta_file" | while read -r header; do
        fasta_id=$(echo "$header" | cut -d' ' -f1 | sed 's/^>//')  # Extract the FASTA ID
        echo -e "$fasta_id\trange\t$chosen_color" >> "$annotation_file"  # Append the entry
    done
done


