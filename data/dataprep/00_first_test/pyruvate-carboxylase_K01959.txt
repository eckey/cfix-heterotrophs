0A023WXU4	A0A023WXU4_STUST	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A024EE28	A0A024EE28_9PSED	 	Biotin carboxylase
A0A024HAM6	A0A024HAM6_PSEKB	 	Biotin carboxylase [EC:6.3.4.14]
A0A059V6F7	A0A059V6F7_PSEPU	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.1]
A0A075WHN3	A0A075WHN3_ARCFL	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.4.1.1]
A0A076FA17	A0A076FA17_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A076LB98	A0A076LB98_9EURY	 	Acetyl-CoA carboxylase, biotin carboxylase
A0A077FFX1	A0A077FFX1_9PSED	 	Pyruvate carboxylase subunit A
A0A077LL65	A0A077LL65_9PSED	 	Pyruvate carboxylase subunit A
A0A089WQP7	A0A089WQP7_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A089YX17	A0A089YX17_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A090I4J0	A0A090I4J0_METFO	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A098G308	A0A098G308_9GAMM	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A098GCC9	A0A098GCC9_LEGMI	 	Biotin carboxylase [EC:6.3.4.14]
A0A099N8P0	A0A099N8P0_PSEDL	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.1]
A0A099U8T9	A0A099U8T9_9HELI	 	Biotin carboxylase [EC:6.3.4.14]
A0A099UD29	A0A099UD29_9HELI	 	Biotin carboxylase [EC:6.3.4.14]
A0A0A7GC65	A0A0A7GC65_GEOAI	 	Biotin carboxylase of acetyl-CoA carboxylase
A0A0A7JU40	A0A0A7JU40_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A0A8GWE7	A0A0A8GWE7_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0A8H3Q5	A0A0A8H3Q5_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0A8HC81	A0A0A8HC81_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0A8HVG2	A0A0A8HVG2_CAMLA	 	Biotin carboxylase [EC:6.3.4.14]
A0A0A8UT85	A0A0A8UT85_LEGHA	 	Biotin carboxylase 1 [EC:6.3.4.14]
A0A0B7IZR4	A0A0B7IZR4_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0C4WIY8	A0A0C4WIY8_9GAMM	 	Pyruvate carboxylase subunit A
A0A0C5E565	A0A0C5E565_9PSED	 	Uncharacterized protein [EC:6.4.1.1]
A0A0C6E0L0	A0A0C6E0L0_9CHLR	 	Biotin carboxylase [EC:6.3.4.14]
A0A0D5XR33	A0A0D5XR33_9PSED	 	Pyruvate carboxylase
A0A0D6BRS5	A0A0D6BRS5_9PSED	 	Pyruvate carboxylase subunit A
A0A0D6EVL2	A0A0D6EVL2_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0E1ED49	A0A0E1ED49_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A0E3HA75	A0A0E3HA75_METTE	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3KT05	A0A0E3KT05_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3L762	A0A0E3L762_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3LCA9	A0A0E3LCA9_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3LM48	A0A0E3LM48_METBA	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3NED7	A0A0E3NED7_METTT	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3NNN7	A0A0E3NNN7_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3P1Q9	A0A0E3P1Q9_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3PAP4	A0A0E3PAP4_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3Q8K3	A0A0E3Q8K3_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3QG74	A0A0E3QG74_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3QYG2	A0A0E3QYG2_METBA	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3R4C3	A0A0E3R4C3_METBA	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3RLX1	A0A0E3RLX1_METMZ	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3RUE1	A0A0E3RUE1_METMZ	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3RZJ9	A0A0E3RZJ9_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3SPV6	A0A0E3SPV6_METMT	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3WV63	A0A0E3WV63_9EURY	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0E3WXC3	A0A0E3WXC3_METBA	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A0F7DBE4	A0A0F7DBE4_9EURY	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.4.1.1]
A0A0F7FGB4	A0A0F7FGB4_9CREN	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A0F7GBM1	A0A0F7GBM1_9CHLR	 	Biotin carboxylase of acetyl-CoA carboxylase [EC:6.3.4.14]
A0A0F7M215	A0A0F7M215_9GAMM	 	Biotin carboxylase [EC:6.4.1.1]
A0A0F7YFN6	A0A0F7YFN6_9PSED	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A0F8W133	A0A0F8W133_LOKSG	 	Pyruvate carboxylase subunit A
A0A0G3G4V3	A0A0G3G4V3_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A0H2ZII0	A0A0H2ZII0_PSEAB	 	Biotin carboxylase [EC:6.3.4.14]
A0A0H3PD54	A0A0H3PD54_CAMJJ	 	Biotin carboxylase [EC:6.3.4.14]
A0A0H4I626	A0A0H4I626_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A0H5ACE8	A0A0H5ACE8_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A0K0SVE2	A0A0K0SVE2_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A0K1HFQ4	A0A0K1HFQ4_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A0K1U9V7	A0A0K1U9V7_9GAMM	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A0K2GAB2	A0A0K2GAB2_NITMO	 	Biotin carboxylase [EC:6.3.4.14]
A0A0M4SRX9	A0A0M4SRX9_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A0N1JUZ0	A0A0N1JUZ0_PSEA0	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.1]
A0A0N1MQ33	A0A0N1MQ33_9HELI	 	Acetyl-CoA carboxylase [EC:6.4.1.2]
A0A0S1SG30	A0A0S1SG30_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A0S2I4M4	A0A0S2I4M4_9BACT	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A0S2KBV8	A0A0S2KBV8_9GAMM	 	Pyruvate carboxylase subunit A
A0A0S2TDY1	A0A0S2TDY1_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A0U2V520	A0A0U2V520_9EURY	 	Pyruvate carboxylase subunit A PycA
A0A0U4HLY9	A0A0U4HLY9_9PSED	 	Pyruvate carboxylase subunit A
A0A0U5B7T8	A0A0U5B7T8_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A0X3BPK3	A0A0X3BPK3_9EURY	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.3.4.14]
A0A109KK15	A0A109KK15_PSEFL	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A109ULD5	A0A109ULD5_9GAMM	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A109UUQ7	A0A109UUQ7_9EURY	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A126QSP4	A0A126QSP4_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A126QY85	A0A126QY85_METOL	 	Pyruvate carboxylase subunit A PycA
A0A126T2V4	A0A126T2V4_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A127HU42	A0A127HU42_PSEAZ	 	Pyruvate carboxylase subunit A
A0A127M6U5	A0A127M6U5_9GAMM	 	Pyruvate carboxylase subunit A
A0A147H7F7	A0A147H7F7_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A161HTH0	A0A161HTH0_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A172UDD8	A0A172UDD8_METSD	 	Biotin carboxylase [EC:6.3.4.14]
A0A172Z9C8	A0A172Z9C8_9PSED	 	Biotin carboxylase
A0A191Z1U8	A0A191Z1U8_9PSED	 	Pyruvate carboxylase subunit A
A0A1A9EU55	A0A1A9EU55_9GAMM	 	Pyruvate carboxylase subunit A
A0A1B1U4I7	A0A1B1U4I7_9HELI	 	Biotin carboxylase [EC:6.3.4.14]
A0A1B3EFE6	A0A1B3EFE6_9PSED	 	Biotin carboxylase [EC:6.3.4.14]
A0A1B3X9G6	A0A1B3X9G6_CAMCO	 	Biotin carboxylase [EC:6.3.4.14]
A0A1B4V4H1	A0A1B4V4H1_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A1B4XFU5	A0A1B4XFU5_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A1C0AU74	A0A1C0AU74_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A1D3L2V0	A0A1D3L2V0_9EURY	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A1D7TM30	A0A1D7TM30_9BACT	 	2-oxoglutarate carboxylase [EC:6.3.4.14]
A0A1D8IT25	A0A1D8IT25_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A1H9HI26	A0A1H9HI26_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A1I0U036	A0A1I0U036_9PSED	 	Biotin carboxylase [EC:6.3.4.14]
A0A1J0VIV7	A0A1J0VIV7_9GAMM	 	Biotin carboxylation domain-containing protein
A0A1L3PZN6	A0A1L3PZN6_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A1L7G778	A0A1L7G778_9PSED	 	Biotin carboxylase [EC:6.3.4.14]
A0A1P8EUM6	A0A1P8EUM6_9PSED	 	Pyruvate carboxylase subunit A
A0A1P8F5U8	A0A1P8F5U8_9CHLR	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A1P8R2G4	A0A1P8R2G4_9GAMM	 	Pyruvate carboxylase subunit A
A0A1S6U8S8	A0A1S6U8S8_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A1V0B2H3	A0A1V0B2H3_9GAMM	 	Pyruvate carboxylase subunit A
A0A1V0RGV5	A0A1V0RGV5_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A1W1I5U5	A0A1W1I5U5_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A1W6BXN2	A0A1W6BXN2_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A1X9SNC6	A0A1X9SNC6_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A1X9SXR7	A0A1X9SXR7_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A1Y0G8E1	A0A1Y0G8E1_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A1Y0HQB5	A0A1Y0HQB5_9BACT	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A1Y0IFN5	A0A1Y0IFN5_9GAMM	 	Pyruvate carboxylase subunit A
A0A1Y6JT00	A0A1Y6JT00_PSEVI	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A1Z4VRG0	A0A1Z4VRG0_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A222FIA4	A0A222FIA4_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A222MY71	A0A222MY71_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A222NZ62	A0A222NZ62_9GAMM	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A223ZI86	A0A223ZI86_9EURY	 	Biotin carboxylase [EC:6.3.4.14]
A0A239IEC9	A0A239IEC9_PSENT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A290HUT2	A0A290HUT2_9BACT	 	2-oxoglutarate carboxylase [EC:6.3.4.14]
A0A291IP67	A0A291IP67_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A2C9EVZ2	A0A2C9EVZ2_PSEPH	 	2-oxoglutarate carboxylase small subunit CfiB [EC:6.4.1.7]
A0A2G1DL56	A0A2G1DL56_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A2H4VFR3	A0A2H4VFR3_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A2H4VPR3	A0A2H4VPR3_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A2K8KWI2	A0A2K8KWI2_MARES	 	Biotin carboxylase [EC:6.3.4.14]
A0A2K8L520	A0A2K8L520_9PROT	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A2K8UC37	A0A2K8UC37_9GAMM	 	Pyruvate carboxylase subunit A
A0A2K8X1K8	A0A2K8X1K8_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A2K8XFL1	A0A2K8XFL1_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A2K9PVL9	A0A2K9PVL9_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A2L0S3M6	A0A2L0S3M6_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A2L1C9W8	A0A2L1C9W8_METMI	 	Biotin carboxylase [EC:6.3.4.14]
A0A2N1J108	A0A2N1J108_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A2R3IXT2	A0A2R3IXT2_PSEAI	 	Biotin carboxylase [EC:6.3.4.14]
A0A2S0JBW5	A0A2S0JBW5_CAMFE	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.1]
A0A2Z3R1F7	A0A2Z3R1F7_9GAMM	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A2Z4L6Y2	A0A2Z4L6Y2_9EURY	 	Pyruvate carboxylase subunit A
A0A2Z4LN76	A0A2Z4LN76_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A2Z5PIW0	A0A2Z5PIW0_METMI	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A2Z5PUV9	A0A2Z5PUV9_METMI	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0A345J4L6	A0A345J4L6_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A345RYB9	A0A345RYB9_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A345YB15	A0A345YB15_9SPHN	 	ATP-grasp domain-containing protein
A0A346S308	A0A346S308_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A347AH19	A0A347AH19_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A347AIN4	A0A347AIN4_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A347TJN1	A0A347TJN1_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A347U6F7	A0A347U6F7_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A385ZAG2	A0A385ZAG2_9PSED	 	Biotin carboxylase [EC:6.3.4.14]
A0A3A9VFT1	A0A3A9VFT1_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A3A9VPX6	A0A3A9VPX6_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A3D8IWV2	A0A3D8IWV2_9HELI	 	Biotin carboxylase [EC:6.3.4.14]
A0A3G1A838	A0A3G1A838_9CREN	 	Pyruvate carboxyl transferase subunit A [EC:6.4.1.1]
A0A3G9D0Q2	A0A3G9D0Q2_9EURY	 	Pyruvate carboxylase, subunit A
A0A3Q9JML1	A0A3Q9JML1_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A3Q9NKP5	A0A3Q9NKP5_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A3Q9S7K7	A0A3Q9S7K7_9CLOT	 	ATP-grasp domain-containing protein
A0A3S8USS8	A0A3S8USS8_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A3T0VWP6	A0A3T0VWP6_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A411MC69	A0A411MC69_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4P6W768	A0A4P6W768_9GAMM	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A4P7C1U9	A0A4P7C1U9_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4P7WYE8	A0A4P7WYE8_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A4P7XID7	A0A4P7XID7_9ALTE	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4P8L2J0	A0A4P8L2J0_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4P9DVN1	A0A4P9DVN1_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4P9UKT8	A0A4P9UKT8_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A4Q0ZRI6	A0A4Q0ZRI6_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A4V1CBK0	A0A4V1CBK0_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A510BC71	A0A510BC71_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5B7SNU6	A0A5B7SNU6_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A5B7X939	A0A5B7X939_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A5B7XBE0	A0A5B7XBE0_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5B9M0A5	A0A5B9M0A5_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5C2H3L1	A0A5C2H3L1_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A5J6QSB5	A0A5J6QSB5_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5J6RMA4	A0A5J6RMA4_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A5K7Y4I4	A0A5K7Y4I4_9GAMM	 	Acetyl-CoA carboxylase subunit alpha
A0A5P2SJ91	A0A5P2SJ91_PSELU	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5P8NZD7	A0A5P8NZD7_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A5Q5BPK7	A0A5Q5BPK7_MYCSS	 	Pyruvate carboxylase [EC:6.4.1.1]
A0A5Q5CM51	A0A5Q5CM51_MYCSJ	 	Pyruvate carboxylase [EC:6.4.1.1]
A0A660HVM6	A0A660HVM6_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A6A7JRN2	A0A6A7JRN2_9BACT	 	Acetyl-CoA carboxylase subunit A
A0A6B9TCM9	A0A6B9TCM9_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A6B9TK04	A0A6B9TK04_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A6F8T9H3	A0A6F8T9H3_9GAMM	 	Pyruvate carboxylase subunit A
A0A6F8VDK2	A0A6F8VDK2_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A6G5QNB0	A0A6G5QNB0_CAMRE	 	Biotin carboxylase [EC:6.3.4.14]
A0A6G7RX71	A0A6G7RX71_9FLAO	 	Biotin carboxylase [EC:6.3.4.14]
A0A6G7VCJ7	A0A6G7VCJ7_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A6H1WQK3	A0A6H1WQK3_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A6H3CV20	A0A6H3CV20_9PSED	 	Pyruvate carboxylase subunit A
A0A6I6EH67	A0A6I6EH67_THETI	 	Biotin carboxylase [EC:6.3.4.14]
A0A6I6JLE6	A0A6I6JLE6_9BACT	 	biotin carboxylase [EC:6.3.4.14]
A0A6I6QJT3	A0A6I6QJT3_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A6I6SS72	A0A6I6SS72_9GAMM	 	Biotin carboxylation domain-containing protein
A0A6J4EDF1	A0A6J4EDF1_9PSED	 	Acetyl-CoA carboxylase subunit alpha
A0A6M8EDK9	A0A6M8EDK9_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A6M8FKS4	A0A6M8FKS4_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A6M8MPY5	A0A6M8MPY5_9PSED	 	2-oxoglutarate carboxylase small subunit [EC:6.4.1.7]
A0A6M8N1Y6	A0A6M8N1Y6_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A6M8NL55	A0A6M8NL55_9BACT	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A0A6N0HXK1	A0A6N0HXK1_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A6N1CE96	A0A6N1CE96_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7D5DA48	A0A7D5DA48_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7D5I4B0	A0A7D5I4B0_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7G1NAJ1	A0A7G1NAJ1_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A7G7RFL2	A0A7G7RFL2_9PSED	 	Biotin carboxylase [EC:6.3.4.14]
A0A7G8WS50	A0A7G8WS50_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A7H1J821	A0A7H1J821_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7H9CIC2	A0A7H9CIC2_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A7H9MQE2	A0A7H9MQE2_9BACT	 	ATP-grasp domain-containing protein
A0A7I7Q7D0	A0A7I7Q7D0_9MYCO	 	biotin carboxylase [EC:6.3.4.14]
A0A7L5HMQ0	A0A7L5HMQ0_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A7L9GJT9	A0A7L9GJT9_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7M1AT27	A0A7M1AT27_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7M1B3A6	A0A7M1B3A6_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7M1B975	A0A7M1B975_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7M1S1L8	A0A7M1S1L8_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A7R7AEX9	A0A7R7AEX9_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A7R7GKU7	A0A7R7GKU7_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A7S7LX11	A0A7S7LX11_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7S7M1H8	A0A7S7M1H8_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7S7RLP3	A0A7S7RLP3_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7S8E8S8	A0A7S8E8S8_9CHLR	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7S8FEU9	A0A7S8FEU9_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A7T1Y1D7	A0A7T1Y1D7_9GAMM	 	Biotin carboxylation domain-containing protein
A0A7T4UNV9	A0A7T4UNV9_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7U6JHK2	A0A7U6JHK2_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A7Y5Z455	A0A7Y5Z455_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A7Z3GNL5	A0A7Z3GNL5_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A809NM41	A0A809NM41_HELCP	 	Biotin carboxylase [EC:6.3.4.14]
A0A809RHN6	A0A809RHN6_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A810KXY1	A0A810KXY1_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A895YFH5	A0A895YFH5_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A8A3S391	A0A8A3S391_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A8D4C8P5	A0A8D4C8P5_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
A0A8D5FMN7	A0A8D5FMN7_9BACT	 	Acetyl-CoA carboxylase subunit alpha
A0A8D5FZE2	A0A8D5FZE2_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
A0A8D6PV60	A0A8D6PV60_9EURY	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.3.4.14]
A0A8E7EJX8	A0A8E7EJX8_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A8F4WZ25	A0A8F4WZ25_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A8T8KAP8	A0A8T8KAP8_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A915YGT5	A0A915YGT5_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
A0A916NXT0	A0A916NXT0_DEHMC	 	Biotin carboxylase [EC:6.3.4.14]
A0A974RXL4	A0A974RXL4_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A975GDF9	A0A975GDF9_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9E6NXM9	A0A9E6NXM9_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A9E6PKV7	A0A9E6PKV7_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A9E6TBG9	A0A9E6TBG9_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A9E6TXH4	A0A9E6TXH4_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A9E6XWS5	A0A9E6XWS5_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A9E7IKU9	A0A9E7IKU9_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9E7PP90	A0A9E7PP90_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9E8MVS9	A0A9E8MVS9_9FLAO	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0A9Q9MJJ5	A0A9Q9MJJ5_9ACTN	 	biotin carboxylase [EC:6.3.4.14]
A0A9X7N3T3	A0A9X7N3T3_PSEDE	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9X7UY61	A0A9X7UY61_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9X9S4J2	A0A9X9S4J2_METOG	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0A9Y2DFY8	A0A9Y2DFY8_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0AA41FUS8	A0AA41FUS8_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0AA49RA20	A0AA49RA20_9PSED	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0AA50ALY0	A0AA50ALY0_9GAMM	 	Acetyl-CoA carboxylase biotin carboxylase subunit [EC:6.4.1.2]
A0AA51UL28	A0AA51UL28_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0AA51YHY8	A0AA51YHY8_9EURY	 	Acetyl-CoA carboxylase biotin carboxylase subunit
A0B7A9	A0B7A9_METTP	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A0LG23	A0LG23_SYNFM	 	Acetyl-coenzyme A carboxylase carboxyl transferase subunit alpha / biotin carboxylase [EC:6.3.4.14]
A0RNR8	A0RNR8_CAMFF	 	Biotin carboxylase [EC:6.3.4.14]
A1RWE9	A1RWE9_THEPD	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A1UL76	A1UL76_MYCSK	 	Pyruvate carboxylase [EC:6.4.1.1]
A2SPQ9	A2SPQ9_METLZ	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A3CW19	A3CW19_METMJ	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A4ASV3	A4ASV3_MARSH	 	Biotin carboxylase [EC:6.3.4.14]
A4FZJ7	A4FZJ7_METM5	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
A4T6U4	A4T6U4_MYCGI	 	Pyruvate carboxylase [EC:6.4.1.1]
A4VFZ2	A4VFZ2_STUS1	 	Acetyl-CoA carboxylase, biotin carboxylase
A4Y0W0	A4Y0W0_PSEMY	 	Biotin carboxylase [EC:6.3.4.14]
A5UL92	A5UL92_METS3	 	Pyruvate carboxylase (Acetyl-CoA/biotin carboxylase), subunit A, PycA [EC:6.3.4.14]
A5WB64	A5WB64_PSEP1	 	Acetyl-CoA carboxylase carboxyltransferase subunit alpha [EC:6.3.4.14]
A6URX6	A6URX6_METVS	 	Acetyl-CoA carboxylase, biotin carboxylase
A6UVR2	A6UVR2_META3	 	Acetyl-CoA carboxylase, biotin carboxylase
A6VEQ0	A6VEQ0_PSEA7	 	Biotin carboxylase [EC:6.3.4.14]
A6VIX9	A6VIX9_METM7	 	Acetyl-CoA carboxylase, biotin carboxylase
A6W294	A6W294_MARMS	 	Carbamoyl-phosphate synthase L chain ATP-binding
A7GW04	A7GW04_CAMC5	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
A7H301	A7H301_CAMJD	 	Biotin carboxylase [EC:6.3.4.14]
A7I1H8	A7I1H8_CAMHC	 	Biotin carboxylase (Acetyl-CoA carboxylase subunit A) (ACC) [EC:6.3.4.14]
A7I988	A7I988_METB6	 	Acetyl-CoA carboxylase, biotin carboxylase
A7ZB04	A7ZB04_CAMC1	 	biotin carboxylase [EC:6.3.4.14]
A8ESE6	A8ESE6_ALIB4	 	Acetyl CoA carboxylase, biotin carboxylase subunit [EC:6.4.1.2]
A9A700	A9A700_METM6	 	Acetyl-CoA carboxylase, biotin carboxylase
B0KR73	B0KR73_PSEPG	 	Acetyl-CoA carboxylase, biotin carboxylase
B1JFL7	B1JFL7_PSEPW	 	Acetyl-CoA carboxylase, biotin carboxylase
B4U8K5	B4U8K5_HYDS0	 	Acetyl-CoA carboxylase, biotin carboxylase
B5YH39	B5YH39_THEYD	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.2]
B8FL79	B8FL79_DESAL	 	Pyruvate carboxylase, biotin carboxylase subunit [EC:6.4.1.1]
B8GGV2	B8GGV2_METPE	 	Carbamoyl-phosphate synthase L chain ATP-binding
B8GSY1	B8GSY1_THISH	 	Biotin carboxylase [EC:6.3.4.14]
B8I8R5	B8I8R5_RUMCH	 	Carbamoyl-phosphate synthase L chain ATP-binding
B9KFT5	B9KFT5_CAMLR	 	Biotin carboxylase [EC:6.3.4.14]
C0QRV5	C0QRV5_PERMH	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.3.4.14]
C1DH59	C1DH59_AZOVD	 	Pyruvate carboxylase subunit A
C1DWH6	C1DWH6_SULAA	 	Pyruvate carboxylase (Pyruvic carboxylase) (PYC) [EC:6.4.1.1]
C3K4F7	C3K4F7_PSEFS	 	Biotin carboxylase [EC:6.4.1.2]
C4LFT8	C4LFT8_TOLAT	 	Acetyl-CoA carboxylase, biotin carboxylase
C6WVV6	C6WVV6_METML	 	Biotin carboxylase [EC:6.3.4.14]
C6XDL2	C6XDL2_METGS	 	Biotin carboxylase [EC:6.3.4.14]
C7P766	C7P766_METFA	 	Acetyl-CoA carboxylase, biotin carboxylase
C9RGN2	C9RGN2_METVM	 	Acetyl-CoA carboxylase, biotin carboxylase
D1B442	D1B442_SULD5	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
D1YUY4	D1YUY4_METPS	 	Putative pyruvate carboxylase subunit A
D2BG52	D2BG52_DEHMV	 	Biotin carboxylase [EC:6.3.4.14]
D2RFN9	D2RFN9_ARCPA	 	Acetyl-CoA carboxylase, biotin carboxylase
D3DJP4	D3DJP4_HYDTT	 	Pyruvate carboxylase small subunit
D3E2D7	D3E2D7_METRM	 	Pyruvate carboxylase subunit A PycA [EC:6.4.1.2]
D3RTU2	D3RTU2_ALLVD	 	Biotin carboxylase [EC:6.3.4.14]
D3S2H2	D3S2H2_FERPA	 	Acetyl-CoA carboxylase, biotin carboxylase
D3S3X4	D3S3X4_METSF	 	Acetyl-CoA carboxylase, biotin carboxylase
D3SAP7	D3SAP7_THISK	 	Biotin carboxylase [EC:6.3.4.14]
D3SLD9	D3SLD9_THEAH	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
D5C4K7	D5C4K7_NITHN	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
D5EAE6	D5EAE6_METMS	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
D5U107	D5U107_THEAM	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
D5V707	D5V707_ARCNC	 	Carbamoyl-phosphate synthase L chain ATP-binding protein [EC:6.3.4.14]
D5VR94	D5VR94_METIM	 	Acetyl-CoA carboxylase, biotin carboxylase
D6Z279	D6Z279_DESAT	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
D7DI29	D7DI29_METV0	 	Biotin carboxylase [EC:6.3.4.14]
D7DR99	D7DR99_METV3	 	Acetyl-CoA carboxylase, biotin carboxylase
D7E897	D7E897_METEZ	 	Acetyl-CoA carboxylase, biotin carboxylase
D8PCK3	D8PCK3_9BACT	 	Biotin carboxylase [EC:6.3.4.14]
D9PV36	D9PV36_METTM	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
E0UU74	E0UU74_SULAO	 	Carbamoyl-phosphate synthase L chain ATP-binding protein [EC:6.3.4.14]
E1RHS7	E1RHS7_METP4	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
E1VB29	E1VB29_HALED	 	Biotin carboxylation domain protein
E3GX30	E3GX30_METFV	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
E4PJY8	E4PJY8_MARAH	 	Acetyl-CoA carboxylase biotin carboxylase subunit
E4U015	E4U015_SULKY	 	Carbamoyl-phosphate synthase L chain ATP-binding protein [EC:6.4.1.1]
E6VW21	E6VW21_PSEA9	 	biotin carboxylase [EC:6.3.4.14]
E8T4P1	E8T4P1_THEA1	 	Acetyl-CoA carboxylase, biotin carboxylase
F0JG37	F0JG37_9BACT	 	biotin carboxylase [EC:6.3.4.14]
F0S1H9	F0S1H9_DESTD	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F0TB64	F0TB64_METLA	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F2K751	F2K751_PSEBN	 	Putative bifunctional protein: arbamoyl-phosphate synthase L chain biotin carboxylase
F2KPK8	F2KPK8_ARCVS	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F2NJF3	F2NJF3_DESAR	 	Biotin carboxylase [EC:6.3.4.14]
F4BTD8	F4BTD8_METSG	 	biotin carboxylase [EC:6.3.4.14]
F6AAD8	F6AAD8_PSEF1	 	Biotin carboxylase [EC:6.3.4.14]
F6BF33	F6BF33_METIK	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F6D7M1	F6D7M1_METPW	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F7XQF3	F7XQF3_METZD	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F8ALC9	F8ALC9_METOI	 	Acetyl-CoA carboxylase, biotin carboxylase [EC:6.4.1.1]
F8G0W1	F8G0W1_PSEP6	 	Pyruvate carboxylase subunit A
F8GJM4	F8GJM4_NITSI	 	Biotin carboxylase [EC:6.3.4.14]
F8H594	F8H594_STUS2	 	Biotin carboxylase [EC:6.3.4.14]
F9ZGJ1	F9ZGJ1_9PROT	 	Biotin carboxylase [EC:6.3.4.14]
G0H2N8	G0H2N8_METMI	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
G2HU10	G2HU10_9BACT	 	Acetyl CoA carboxylase biotin carboxylase subunit
G4SUW1	G4SUW1_META2	 	Biotin carboxylase [EC:6.3.4.14]
G7WLD2	G7WLD2_METH6	 	Pyruvate carboxylase subunit A
G8Q839	G8Q839_PSEO1	 	AccC [EC:6.3.4.14]
H8WAS8	H8WAS8_MARN1	 	Biotin carboxylase [EC:6.3.4.14]
I1XIQ7	I1XIQ7_METNJ	 	Biotin carboxylase [EC:6.3.4.14]
I1YJX5	I1YJX5_METFJ	 	Biotin carboxylase [EC:6.3.4.14]
I3TEX6	I3TEX6_THEC1	 	Carbamoyl-phosphate synthase L chain, ATP-binding protein
I3UVB7	I3UVB7_PSEPU	 	Pyruvate carboxylase subunit A
I3XS45	I3XS45_DESAM	 	Carbamoyl-phosphate synthase L chain ATP-binding protein
I3Y572	I3Y572_THIV6	 	Pyruvate carboxylase
I4CYX9	I4CYX9_STUST	 	Biotin carboxylase [EC:6.3.4.14]
I7B406	I7B406_PSEPT	 	Acetyl-CoA carboxylase
I7HCY3	I7HCY3_9HELI	 	Biotin carboxylase [EC:6.3.4.14]
I7L0I6	I7L0I6_METBM	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
K7SBJ6	K7SBJ6_9BACT	 	Acetyl-CoA carboxylase biotin carboxylase subunit
K9NTM8	K9NTM8_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
L0DVK2	L0DVK2_THIND	 	Biotin carboxylase [EC:6.3.4.14]
L0GT54	L0GT54_STUST	 	Biotin carboxylase
L0GY33	L0GY33_9GAMM	 	Acetyl/propionyl-CoA carboxylase, alpha subunit
L0HC90	L0HC90_METFS	 	Acetyl-CoA carboxylase, biotin carboxylase subunit
L0KUC7	L0KUC7_METHD	 	Acetyl-CoA carboxylase, biotin carboxylase subunit
M1FER3	M1FER3_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
M1P7I9	M1P7I9_DESSD	 	biotin carboxylase [EC:6.3.4.14]
M1PY52	M1PY52_METMZ	 	Pyruvate carboxyl transferase subunit A
M1R756	M1R756_9AQUI	 	Acetyl-CoA carboxylase, biotin carboxylase
M1WJI6	M1WJI6_PSEP2	 	biotin carboxylase [EC:6.3.4.14]
M4MJ10	M4MJ10_CAMJU	 	Biotin carboxylase [EC:6.3.4.14]
M4X6W3	M4X6W3_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
M5DVZ4	M5DVZ4_9GAMM	 	2-oxoglutarate carboxylase small subunit
M9Y9S9	M9Y9S9_AZOVI	 	Pyruvate carboxylase subunit A
N0BGB8	N0BGB8_9EURY	 	Pyruvate carboxylase subunit A
O27939	PYCA_METTH	*	Pyruvate carboxylase subunit A [EC:6.4.1.1]
O30019	PYCA_ARCFU	*	Pyruvate carboxylase subunit A [EC:6.4.1.1]
O67449	O67449_AQUAE	 	Biotin carboxylase
Q0P9L5	Q0P9L5_CAMJE	 	Biotin carboxylase [EC:6.3.4.14]
Q0W661	Q0W661_METAR	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
Q12TE9	Q12TE9_METBU	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q1H158	Q1H158_METFK	 	Biotin carboxylase [EC:6.3.4.14]
Q1I2N0	Q1I2N0_PSEE4	 	Biotin carboxylase (A subunit of acetyl-CoA carboxylase) [EC:6.4.1.1]
Q1QXA0	Q1QXA0_CHRSD	 	Carbamoyl-phosphate synthase L chain, ATP-binding protein
Q2FNH4	Q2FNH4_METHJ	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q2LTP0	Q2LTP0_SYNAS	 	Pyruvate carboxylase biotin carboxylase subunit [EC:6.4.1.1]
Q2NFV1	Q2NFV1_METST	 	PycA [EC:6.4.1.1]
Q2S7P1	Q2S7P1_HAHCH	 	Biotin carboxylase [EC:6.3.4.14]
Q30SY0	Q30SY0_SULDN	 	Pyruvate carboxylase biotin carboxylase subunit
Q3K4C8	Q3K4C8_PSEPF	 	Acetyl-CoA carboxylase carboxyltransferase subunit alpha [EC:6.4.1.1]
Q3SIM0	Q3SIM0_THIDA	 	Biotin carboxylase [EC:6.3.4.14]
Q3ZA79	Q3ZA79_DEHM1	 	Biotin carboxylase [EC:6.3.4.14]
Q46C58	Q46C58_METBF	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q48BM5	Q48BM5_PSE14	 	Acetyl-CoA carboxylase, biotin carboxylase, putative
Q4K3G7	Q4K3G7_PSEF5	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.3.4.14]
Q4ZL84	Q4ZL84_PSEU2	 	Acetyl-CoA carboxylase carboxyltransferase subunit alpha / biotin carboxylase [EC:6.3.4.14]
Q58626	PYCA_METJA	*	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q5WTD4	Q5WTD4_LEGPL	 	Uncharacterized protein
Q5ZS55	Q5ZS55_LEGPH	 	Acetyl-CoA carboxylase, biotin carboxylase subunit [EC:6.4.1.1]
Q6M0D0	Q6M0D0_METMP	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q7M900	Q7M900_WOLSU	 	Biotin carboxylase [EC:6.3.4.14]
Q7VF30	Q7VF30_HELHP	 	Biotin carboxylase [EC:6.3.4.14]
Q87U06	Q87U06_PSESM	 	Acetyl-CoA carboxylase, biotin carboxylase, putative
Q88C36	Q88C36_PSEPK	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
Q8PVX7	Q8PVX7_METMA	 	Pyruvate carboxylase, subunit A [EC:6.4.1.1]
Q8TSX0	Q8TSX0_METAC	 	Pyruvate carboxylase subunit A
Q9HTD0	Q9HTD0_PSEAE	 	Biotin carboxylase [EC:6.3.4.14]
R4YLL7	R4YLL7_OLEAN	 	Biotin carboxylase [EC:6.3.4.14]
R9SNA6	R9SNA6_9EURY	 	Pyruvate carboxylase subunit A PycA
S5ZUM2	S5ZUM2_9CREN	 	Acetyl-CoA carboxylase biotin carboxylase subunit
S6AH10	S6AH10_SULDS	 	Biotin carboxylase [EC:6.3.4.14]
S6AXU5	S6AXU5_PSERE	 	Biotin carboxylase [EC:6.3.4.14]
S6CG86	S6CG86_9GAMM	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
T2GKM0	T2GKM0_9EURY	 	Pyruvate carboxylase subunit A
U6ECW5	U6ECW5_9EURY	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
V9V8B4	V9V8B4_9PSED	 	Acetyl-CoA carboxylase subunit alpha [EC:6.4.1.1]
W0B889	W0B889_9GAMM	 	Acetyl/propionyl-CoA carboxylase, alpha, subunit [EC:6.4.1.1]
W0DG36	W0DG36_9AQUI	 	Acetyl-CoA carboxylase
W0DLQ5	W0DLQ5_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
W0E5Q4	W0E5Q4_MARPU	 	Biotin carboxylase [EC:6.3.4.14]
W0MXY0	W0MXY0_PSESX	 	Acetyl-CoA carboxylase subunit alpha [EC:6.4.1.1]
W6RLU2	W6RLU2_PSEP5	 	Biotin carboxylase [EC:6.3.4.14]
W8KHT6	W8KHT6_9GAMM	 	Biotin carboxylase [EC:6.3.4.14]
W8QKE3	W8QKE3_9PSED	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
W8R646	W8R646_STUST	 	Pyruvate carboxylase subunit A [EC:6.4.1.1]
